﻿using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.GUI
{
    public class TargetLinkPopupResourceGroup : ResourceGroup
    {
        public TargetLinkPopupResourceGroup()
        {
            AddFile("Functions.js");
            AddFile("TargetLinkPopup.js");

            AddFile("ComponentTransformerPopup.css");

            Dependencies.AddLibraryJQuery();
            Dependencies.Add("Tridion.Web.UI.Editors.CME");
            Dependencies.Add("Tridion.Web.UI.Editors.CME.commands");

            AddWebApiProxy();

            AttachToView("TargetLinkPopup.aspx");
        }
    }
}