﻿using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.GUI
{
    public class NameTransformPopupResourceGroup : ResourceGroup
    {
        public NameTransformPopupResourceGroup()
        {
            AddFile("Functions.js");
            AddFile("NameTransformPopup.js");

            AddFile("ComponentTransformerPopup.css");

            Dependencies.AddLibraryJQuery();
            Dependencies.Add("Tridion.Web.UI.Editors.CME");
            Dependencies.Add("Tridion.Web.UI.Editors.CME.commands");

            AddWebApiProxy();

            AttachToView("NameTransformPopup.aspx");
        }
    }
}