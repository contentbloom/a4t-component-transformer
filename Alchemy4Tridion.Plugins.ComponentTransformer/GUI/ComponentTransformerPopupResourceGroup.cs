﻿using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.GUI
{
    public class ComponentTransformerPopupResourceGroup : ResourceGroup
    {
        public ComponentTransformerPopupResourceGroup()
        {
            AddFile("Functions.js");
            AddFile("ComponentTransformerPopup.js");

            AddFile("ComponentTransformerPopup.css");

            Dependencies.AddLibraryJQuery();
            Dependencies.Add("Tridion.Web.UI.Editors.CME");
            Dependencies.Add("Tridion.Web.UI.Editors.CME.commands");

            AddWebApiProxy();

            AttachToView("ComponentTransformerPopup.aspx");
        }
    }
}
