﻿using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.GUI
{
    public class ComponentTransformerRibbonToolbarButton : RibbonToolbarExtension
    {
        public ComponentTransformerRibbonToolbarButton()
        {
            // The unique identifier used for the html element created.
            AssignId = "ComponentTransformerButton";

            // Using command instead of .ascx so we can position it correctly
            Command = "ComponentTransformer";

            // The label of the button.
            Name = "Component Transformer";

            // The page tab to assign this extension to. See Constants.PageIds.
            PageId = Constants.PageIds.HomePage;

            // Option GroupId, put this into an existing group (not capable if using a .ascx Control)
            GroupId = Constants.GroupIds.HomePage.EditGroup;
            InsertBefore = "UndoCheckOutBtn";

            // The tooltip label that will get applied.
            Title = "Component Transformer";

            // We need to add our resource group as a dependency to this extension
            Dependencies.Add<ComponentTransformerResourceGroup>();

            // apply the extension to a specific view.
            Apply.ToView(Constants.Views.DashboardView, "DashboardToolbar");
        }
    }
}
