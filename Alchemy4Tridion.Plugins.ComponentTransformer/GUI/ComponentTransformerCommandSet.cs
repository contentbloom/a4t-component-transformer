﻿using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.GUI
{
    public class ComponentTransformerCommandSet : CommandSet
    {
        public ComponentTransformerCommandSet()
        {
            AddCommand("ComponentTransformer");
        }
    }
}
