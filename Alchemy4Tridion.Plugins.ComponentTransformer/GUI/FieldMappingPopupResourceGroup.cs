﻿using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.GUI
{
    public class FieldMappingPopupResourceGroup : ResourceGroup
    {
        public FieldMappingPopupResourceGroup()
        {
            AddFile("Functions.js");
            AddFile("FieldMappingPopup.js");

            AddFile("ComponentTransformerPopup.css");

            Dependencies.AddLibraryJQuery();
            Dependencies.Add("Tridion.Web.UI.Editors.CME");
            Dependencies.Add("Tridion.Web.UI.Editors.CME.commands");

            AddWebApiProxy();

            AttachToView("FieldMappingPopup.aspx");
        }
    }
}