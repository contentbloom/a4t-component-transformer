﻿using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.GUI
{
    public class ComponentTransformerResourceGroup : ResourceGroup
    {
        public ComponentTransformerResourceGroup()
        {
            AddFile("ComponentTransformerCommand.js");
            AddFile("Styles.css");

            AddFile<ComponentTransformerCommandSet>();

            AddWebApiProxy();
        }
    }
}
