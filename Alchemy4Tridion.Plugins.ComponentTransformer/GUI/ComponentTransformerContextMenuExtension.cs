﻿using Alchemy4Tridion.Plugins.GUI.Configuration;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.GUI
{
    public class ComponentTransformerContextMenuExtension : ContextMenuExtension
    {
        public ComponentTransformerContextMenuExtension()
        {
            AssignId = "ComponentTransformer";

            // The name of the extension menu
            Name = "ComponentTransformerMenu";

            // Use this property to specify where in the context menu your items will go
            InsertBefore = Constants.ContextMenuIds.MainContextMenu.Versioning;

            // Use AddItem() or AddSubMenu() to add items for this context menu

            //       element id      title        command name
            AddItem("cmp_transform_cm", "Component Transformer...", "ComponentTransformer");

            // We need to add our resource group as a dependency to this extension
            Dependencies.Add<ComponentTransformerResourceGroup>();

            // apply the extension to a specific view.
            Apply.ToView(Constants.Views.DashboardView);
        }
    }
}
