﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Alchemy4Tridion.Plugins.ComponentTransformer.Models;
using Alchemy4Tridion.Plugins.Clients;
using Alchemy4Tridion.Plugins.Clients.CoreService;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.Helpers
{
    public static class MainHelper
    {
        #region Tridion items access

        public static string CreateFolder(IAlchemyCoreServiceClient client, string title, string tcmContainer)
        {
            try
            {
                FolderData folderData = new FolderData
                {
                    Title = title,
                    LocationInfo = new LocationInfo { OrganizationalItem = new LinkToOrganizationalItemData { IdRef = tcmContainer } },
                    Id = "tcm:0-0-0"
                };

                folderData = client.Create(folderData, new ReadOptions()) as FolderData;
                if (folderData == null)
                    return string.Empty;

                return folderData.Id;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private static string CreateFolderChain(IAlchemyCoreServiceClient client, List<string> folderChain, string tcmContainer)
        {
            if (folderChain == null || folderChain.Count == 0 || string.IsNullOrEmpty(tcmContainer))
                return tcmContainer;

            string topFolder = folderChain[0];
            List<ItemInfo> items = GetFoldersByParentFolder(client, tcmContainer);
            if (items.All(x => x.Title != topFolder))
            {
                CreateFolder(client, topFolder, tcmContainer);
                items = GetFoldersByParentFolder(client, tcmContainer);
            }

            string tcmTopFolder = items.First(x => x.Title == topFolder).TcmId;

            return CreateFolderChain(client, folderChain.Skip(1).ToList(), tcmTopFolder);
        }

        public static ComponentData GetComponent(IAlchemyCoreServiceClient client, string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            return ReadItem(client, id) as ComponentData;
        }

        public static IdentifiableObjectData ReadItem(IAlchemyCoreServiceClient client, string id)
        {
            try
            {
                return client.Read(id, null);
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        #region Tridion hierarchy

        public static List<ItemInfo> GetItemsByParentContainer(IAlchemyCoreServiceClient client, string tcmContainer)
        {
            return client.GetListXml(tcmContainer, new OrganizationalItemItemsFilterData()).ToList();
        }

        public static List<ItemInfo> GetItemsByParentContainer(IAlchemyCoreServiceClient client, string tcmContainer, bool recursive)
        {
            return client.GetListXml(tcmContainer, new OrganizationalItemItemsFilterData { Recursive = recursive }).ToList();
        }

        public static List<ItemInfo> GetItemsByParentContainer(IAlchemyCoreServiceClient client, string tcmContainer, bool recursive, ItemType[] itemTypes)
        {
            return client.GetListXml(tcmContainer, new OrganizationalItemItemsFilterData { Recursive = recursive, ItemTypes = itemTypes }).ToList();
        }

        public static List<ItemInfo> GetFoldersByParentFolder(IAlchemyCoreServiceClient client, string tcmFolder)
        {
            return client.GetListXml(tcmFolder, new OrganizationalItemItemsFilterData { ItemTypes = new[] { ItemType.Folder } }).ToList(ItemType.Folder);
        }

        public static List<ItemInfo> GetFolders(IAlchemyCoreServiceClient client, string tcmFolder, bool recursive)
        {
            return client.GetListXml(tcmFolder, new OrganizationalItemItemsFilterData { ItemTypes = new[] { ItemType.Folder }, Recursive = recursive }).ToList(ItemType.Folder);
        }

        public static List<ItemInfo> GetTbbsByParentFolder(IAlchemyCoreServiceClient client, string tcmFolder)
        {
            return client.GetListXml(tcmFolder, new OrganizationalItemItemsFilterData { ItemTypes = new[] { ItemType.TemplateBuildingBlock } }).ToList(ItemType.TemplateBuildingBlock);
        }

        public static List<ItemInfo> GetStructureGroupsByParentStructureGroup(IAlchemyCoreServiceClient client, string tcmSG)
        {
            return client.GetListXml(tcmSG, new OrganizationalItemItemsFilterData { ItemTypes = new[] { ItemType.StructureGroup } }).ToList(ItemType.StructureGroup);
        }

        public static List<ItemInfo> GetFoldersByPublication(IAlchemyCoreServiceClient client, string tcmPublication)
        {
            return client.GetListXml(tcmPublication, new RepositoryItemsFilterData { ItemTypes = new[] { ItemType.Folder } }).ToList(ItemType.Folder);
        }

        public static List<ItemInfo> GetStructureGroupsByPublication(IAlchemyCoreServiceClient client, string tcmPublication)
        {
            return client.GetListXml(tcmPublication, new RepositoryItemsFilterData { ItemTypes = new[] { ItemType.StructureGroup } }).ToList(ItemType.StructureGroup);
        }

        public static List<ItemInfo> GetContainersByPublication(IAlchemyCoreServiceClient client, string tcmPublication)
        {
            return client.GetListXml(tcmPublication, new RepositoryItemsFilterData { ItemTypes = new[] { ItemType.Folder, ItemType.StructureGroup } }).ToList();
        }

        public static List<ItemInfo> GetCategoriesByPublication(IAlchemyCoreServiceClient client, string tcmPublication)
        {
            return client.GetListXml(tcmPublication, new RepositoryItemsFilterData { ItemTypes = new[] { ItemType.Category } }).ToList();
        }

        public static List<ItemInfo> GetKeywordsByCategory(IAlchemyCoreServiceClient client, string tcmCategory)
        {
            return client.GetListXml(tcmCategory, new OrganizationalItemItemsFilterData { ItemTypes = new[] { ItemType.Keyword } }).ToList(ItemType.Keyword);
        }

        public static List<ItemInfo> GetProcessDefinitionsByPublication(IAlchemyCoreServiceClient client, string tcmPublication)
        {
            return client.GetSystemWideListXml(new ProcessDefinitionsFilterData { ContextRepository = new LinkToRepositoryData { IdRef = tcmPublication } }).ToList(ItemType.ProcessDefinition);
        }

        public static List<ItemInfo> GetItemsByPublication(IAlchemyCoreServiceClient client, string tcmPublication)
        {
            List<ItemInfo> list = new List<ItemInfo>();

            list.AddRange(GetContainersByPublication(client, tcmPublication));

            if (GetCategoriesByPublication(client, tcmPublication).Any())
                list.Add(new ItemInfo { Title = "Categories and Keywords", TcmId = "catman-" + tcmPublication, ItemType = ItemType.Folder });

            if (GetProcessDefinitionsByPublication(client, tcmPublication).Any())
                list.Add(new ItemInfo { Title = "Process Definitions", TcmId = "proc-" + tcmPublication, ItemType = ItemType.Folder });

            return list;
        }

        public static List<ItemInfo> GetItemsByPublication(IAlchemyCoreServiceClient client, string tcmPublication, bool recursive)
        {
            if (!recursive)
                return GetItemsByPublication(client, tcmPublication);

            List<ItemInfo> list = new List<ItemInfo>();

            foreach (ItemInfo container in GetContainersByPublication(client, tcmPublication))
            {
                list.Add(container);
                list.AddRange(GetItemsByParentContainer(client, container.TcmId, true));
            }

            List<ItemInfo> categories = GetCategoriesByPublication(client, tcmPublication);
            if (categories.Any())
            {
                list.Add(new ItemInfo { Title = "Categories and Keywords", TcmId = "catman-" + tcmPublication, ItemType = ItemType.Folder });
                list.AddRange(categories);
            }

            List<ItemInfo> processDefinitions = GetProcessDefinitionsByPublication(client, tcmPublication);
            if (processDefinitions.Any())
            {
                list.Add(new ItemInfo { Title = "Process Definitions", TcmId = "proc-" + tcmPublication, ItemType = ItemType.Folder });
                list.AddRange(processDefinitions);
            }

            return list;
        }

        public static List<ItemInfo> GetPublications(IAlchemyCoreServiceClient client)
        {
            return client.GetSystemWideListXml(new PublicationsFilterData()).ToList(ItemType.Publication);
        }

        public static List<ItemInfo> GetPublications(IAlchemyCoreServiceClient client, string filterItemId)
        {
            List<ItemInfo> publications = GetPublications(client);
            var allowedPublications = client.GetSystemWideList(new BluePrintFilterData { ForItem = new LinkToRepositoryLocalObjectData { IdRef = filterItemId } }).Cast<BluePrintNodeData>().Where(x => x.Item != null).Select(x => GetPublicationTcmId(x.Item.Id)).ToList();
            return publications.Where(x => allowedPublications.Any(y => y == x.TcmId)).ToList();
        }

        public static string GetWebDav(this RepositoryLocalObjectData item)
        {
            if (item.LocationInfo == null || string.IsNullOrEmpty(item.LocationInfo.WebDavUrl))
                return string.Empty;

            string webDav = HttpUtility.UrlDecode(item.LocationInfo.WebDavUrl.Replace("/webdav/", string.Empty));
            if (string.IsNullOrEmpty(webDav))
                return string.Empty;

            int dotIndex = webDav.LastIndexOf(".", StringComparison.Ordinal);
            int slashIndex = webDav.LastIndexOf("/", StringComparison.Ordinal);

            return dotIndex >= 0 && dotIndex > slashIndex ? webDav.Substring(0, dotIndex) : webDav;
        }

        public static List<string> GetWebDavChain(this RepositoryLocalObjectData item)
        {
            List<string> res = item.LocationInfo.WebDavUrl.Replace("/webdav/", string.Empty).Split('/').Select(HttpUtility.UrlDecode).ToList();

            if (res.Last().Contains("."))
                res[res.Count - 1] = res.Last().Substring(0, res.Last().LastIndexOf(".", StringComparison.Ordinal));
            
            return res;
        }

        public static List<string> Substract(this List<string> input, List<string> toSubstract)
        {
            if (input == null || toSubstract == null)
                return input;

            return String.Join("|||", input).Replace(String.Join("|||", toSubstract), string.Empty).Split(new [] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static string GetItemContainer(IAlchemyCoreServiceClient client, string tcmItem)
        {
            RepositoryLocalObjectData item = client.Read(tcmItem, new ReadOptions()) as RepositoryLocalObjectData;
            if (item == null)
                return string.Empty;

            return item.LocationInfo.OrganizationalItem.IdRef;
        }

        #endregion

        #region Tridion schemas

        public static List<ItemInfo> GetSchemas(IAlchemyCoreServiceClient client, string tcmPublication)
        {
            ItemInfo folder0 = GetFoldersByPublication(client, tcmPublication)[0];
            return client.GetListXml(folder0.TcmId, new OrganizationalItemItemsFilterData { Recursive = true, ItemTypes = new[] { ItemType.Schema }, SchemaPurposes = new[] { SchemaPurpose.Component, SchemaPurpose.Metadata } }).ToList(ItemType.Schema);
        }

        public static List<ItemInfo> GetSchemas(IAlchemyCoreServiceClient client, string tcmFolder, bool recursive)
        {
            return client.GetListXml(tcmFolder, new OrganizationalItemItemsFilterData { Recursive = recursive, ItemTypes = new[] { ItemType.Schema } }).ToList(ItemType.Schema);
        }

        public static List<ItemFieldDefinitionData> GetSchemaFields(IAlchemyCoreServiceClient client, string tcmSchema)
        {
            SchemaFieldsData schemaFieldsData = client.ReadSchemaFields(tcmSchema, true, null);
            if (schemaFieldsData == null || schemaFieldsData.Fields == null)
                return null;

            return schemaFieldsData.Fields.ToList();
        }

        public static List<ItemFieldDefinitionData> GetSchemaMetadataFields(IAlchemyCoreServiceClient client, string tcmSchema)
        {
            SchemaFieldsData schemaFieldsData = client.ReadSchemaFields(tcmSchema, true, null);
            if (schemaFieldsData == null || schemaFieldsData.MetadataFields == null)
                return null;

            return schemaFieldsData.MetadataFields.ToList();
        }

        #endregion

        #region Tridion components

        public static List<ItemInfo> GetComponents(IAlchemyCoreServiceClient client, string tcmSchema)
        {
            return client.GetListXml(tcmSchema, new UsingItemsFilterData { ItemTypes = new[] { ItemType.Component } }).ToList(ItemType.Component);
        }

        public static List<ItemInfo> GetComponents(IAlchemyCoreServiceClient client, string tcmFolder, bool recursive)
        {
            return client.GetListXml(tcmFolder, new OrganizationalItemItemsFilterData { ItemTypes = new[] { ItemType.Component }, Recursive = recursive }).ToList(ItemType.Component);
        }

        public static List<ItemInfo> GetComponents(IAlchemyCoreServiceClient client, string tcmFolder, string tcmSchema)
        {
            if (string.IsNullOrEmpty(tcmFolder) && string.IsNullOrEmpty(tcmFolder))
                return new List<ItemInfo>();

            if (string.IsNullOrEmpty(tcmFolder))
                return GetComponents(client, tcmSchema);

            if (string.IsNullOrEmpty(tcmSchema))
                return GetComponents(client, tcmFolder, true);

            return client.GetListXml(tcmFolder, new OrganizationalItemItemsFilterData { ItemTypes = new[] { ItemType.Component }, Recursive = true, BasedOnSchemas = new [] { new LinkToSchemaData { IdRef = tcmSchema} } }).ToList(ItemType.Component);
        }

        public static List<FieldMappingInfo> GetDefaultFieldMapping(IAlchemyCoreServiceClient client, SchemaData targetSchema, List<ItemFieldDefinitionData> targetFields, List<ItemFieldDefinitionData> targetMetadataFields)
        {
            List<FieldInfo> allFields = GetAllFields(client, targetSchema, targetFields, targetMetadataFields, false);

            List<FieldMappingInfo> fieldMapping = new List<FieldMappingInfo>();
            foreach (FieldInfo field in allFields)
            {
                fieldMapping.Add(new FieldMappingInfo {
                    SourceFields = allFields,
                    Id = field.FieldPath.Replace("/", "_").Replace(" ", "_"),
                    SourceFieldPath = field.FieldPath,
                    TargetFields = allFields,
                    TargetFieldName = field.FieldName,
                    TargetFieldPath = field.FieldPath,
                    Level = field.Level,
                    Primitive = field.Primitive,
                    Embedded = field.Embedded,
                    Link = field.Link,
                    Mandatory = field.Mandatory,
                    MultiValue = field.MultiValue,
                    FieldType = field.FieldType,
                    Index = field.Index
                });
            }

            return fieldMapping;
        }

        private static List<FieldMappingInfo> GetNestedFieldMapping(this List<FieldMappingInfo> plainFieldMapping, string parentIndex, int level)
        {
            if (plainFieldMapping == null || string.IsNullOrEmpty(parentIndex))
                return null;

            List<FieldMappingInfo> levelMapping = plainFieldMapping.Where(x => x.Level == level && (level == 0 || level > 0 && x.Index.StartsWith(parentIndex))).ToList();
            if (levelMapping.Count() == 0)
                return null;

            foreach (FieldMappingInfo mapping in levelMapping)
            {
                mapping.ChildFieldMapping = GetNestedFieldMapping(plainFieldMapping, mapping.Index, level + 1);
            }

            return levelMapping;
        }

        private static bool AnyDefaultValue(List<FieldMappingInfo> fieldMapping)
        {
            if (fieldMapping == null)
                return false;

            foreach (FieldMappingInfo mappingItem in fieldMapping)
            {
                if (!string.IsNullOrEmpty(mappingItem.DefaultValue))
                    return true;

                if(AnyDefaultValue(mappingItem.ChildFieldMapping))
                    return true;
            }

            return false;
        }

        private static List<FieldInfo> ExpandChildFields(IAlchemyCoreServiceClient client, FieldInfo field)
        {
            if (field == null || field.Field == null || !field.Field.IsEmbedded() || ((EmbeddedSchemaFieldDefinitionData)field.Field).EmbeddedFields == null)
                return null;

            List<FieldInfo> res = new List<FieldInfo>();

            int index = 0; 
            foreach (ItemFieldDefinitionData item in ((EmbeddedSchemaFieldDefinitionData)field.Field).EmbeddedFields) {

                FieldInfo childField = new FieldInfo();
                childField.FieldName = item.GetFieldName();
                childField.FieldPath = field.FieldPath + "/" + item.Name;
                childField.Level = field.Level + 1;
                childField.Primitive = item.IsPrimitive();
                childField.Embedded = item.IsEmbedded();
                childField.Link = item.IsLink();
                childField.Mandatory = item.IsMandatory();
                childField.MultiValue = item.IsMultiValue();
                childField.FieldType = item.GetFieldType().ToString();
                childField.Field = item;
                childField.Index = field.Index + "." + index;
                childField.Ns = field.Ns;
                res.Add(childField);

                index++;

                if (item.IsEmbedded())
                {
                    var children = ExpandChildFields(client, childField);
                    if (children != null)
                        res.AddRange(children);
                }

                if (item.IsComponentLink())
                {
                    var children = ExpandClFields(client, childField);
                    if (children != null)
                    {
                        res.AddRange(children);
                        childField.Embedded = true;
                    }
                }
            }

            return res;
        }

        private static List<FieldInfo> ExpandClFields(IAlchemyCoreServiceClient client, FieldInfo field)
        {
            if (field == null || field.Field == null || !field.Field.IsComponentLink() || !((ComponentLinkFieldDefinitionData)field.Field).AllowedTargetSchemas.Any())
                return null;

            string childSchemaId = ((ComponentLinkFieldDefinitionData)field.Field).AllowedTargetSchemas[0].IdRef;

            SchemaData schema = ReadItem(client, childSchemaId) as SchemaData;
            if (schema == null)
                return null;

            List<ItemFieldDefinitionData> schemaFields = GetSchemaFields(client, childSchemaId);
            if (schemaFields == null)
                return null;

            List<FieldInfo> res = new List<FieldInfo>();

            int index = 0;
            foreach (ItemFieldDefinitionData item in schemaFields) {

                FieldInfo childField = new FieldInfo();
                childField.FieldName = item.GetFieldName();
                childField.FieldPath = schema.RootElementName + "/" + item.Name;
                childField.Level = field.Level + 1;
                childField.Primitive = item.IsPrimitive();
                childField.Embedded = item.IsEmbedded();
                childField.Link = item.IsLink();
                childField.Mandatory = item.IsMandatory();
                childField.MultiValue = item.IsMultiValue();
                childField.FieldType = item.GetFieldType().ToString();
                childField.Field = item;
                childField.Index = field.Index + "." + index;
                childField.Ns = schema.NamespaceUri;
                res.Add(childField);

                index++;

                if (item.IsEmbedded())
                {
                    var children = ExpandChildFields(client, childField);
                    if(children != null)
                        res.AddRange(children);
                }

                if (item.IsComponentLink())
                {
                    var children = ExpandClFields(client, childField);
                    if (children != null)
                    {
                        res.AddRange(children);
                        childField.Embedded = true;
                    }
                }
            }

            return res;
        }

        public static List<FieldInfo> GetAllFields(IAlchemyCoreServiceClient client, SchemaData schema, List<ItemFieldDefinitionData> schemaFields, List<ItemFieldDefinitionData> metadataFields, bool includeSystemItems)
        {
            List<FieldInfo> res = new List<FieldInfo>();

            if (includeSystemItems)
            {
                //ignore item
                res.Add(new FieldInfo { FieldName = "< ignore >", FieldPath = "" });
            }

            int index = 0;
            if (schemaFields != null)
            {
                foreach (ItemFieldDefinitionData item in schemaFields)
                {
                    FieldInfo field = new FieldInfo();
                    field.FieldName = item.GetFieldName();
                    field.Level = 0;
                    field.FieldPath = schema.RootElementName + "/" + item.Name;
                    field.Primitive = item.IsPrimitive();
                    field.Embedded = item.IsEmbedded();
                    field.Link = item.IsLink();
                    field.Mandatory = item.IsMandatory();
                    field.MultiValue = item.IsMultiValue();
                    field.FieldType = item.GetFieldType().ToString();
                    field.Field = item;
                    field.Index = index.ToString();
                    field.Ns = schema.NamespaceUri;
                    res.Add(field);

                    index++;

                    if (item.IsEmbedded())
                    {
                        var children = ExpandChildFields(client, field);
                        if(children != null)
                            res.AddRange(children);
                    }

                    if (item.IsComponentLink())
                    {
                        var children = ExpandClFields(client, field);
                        if (children != null)
                        {
                            res.AddRange(children);
                            field.Embedded = true;
                        }
                    }
                }
            }

            index = 0;
            if (metadataFields != null)
            {
                if (schemaFields != null && schemaFields.Any() && metadataFields != null && metadataFields.Any())
                {
                    res.Add(new FieldInfo { FieldName = "< metadata >", FieldPath = "metadata" });
                }

                foreach (ItemFieldDefinitionData item in metadataFields)
                {
                    FieldInfo field = new FieldInfo();
                    field.FieldName = item.GetFieldName();
                    field.Level = 0;
                    field.FieldPath = "Metadata/" + item.Name;
                    field.Primitive = item.IsPrimitive();
                    field.Embedded = item.IsEmbedded();
                    field.Link = item.IsLink();
                    field.Mandatory = item.IsMandatory();
                    field.MultiValue = item.IsMultiValue();
                    field.FieldType = item.GetFieldType().ToString();
                    field.Field = item;
                    field.Index = "m." + index;
                    field.Ns = schema.NamespaceUri;
                    res.Add(field);

                    index++;

                    if (item.IsEmbedded())
                    {
                        var children = ExpandChildFields(client, field);
                        if (children != null)
                            res.AddRange(children);
                    }

                    if (item.IsComponentLink())
                    {
                        var children = ExpandClFields(client, field);
                        if (children != null)
                        {
                            res.AddRange(children);
                            field.Embedded = true;
                        }
                    }
                }
            }

            if (includeSystemItems)
            {
                //new empty embedded schema, with possible child values
                res.Add(new FieldInfo { FieldName = "< new >", FieldPath = "create_new" });
                //link to source component
                res.Add(new FieldInfo { FieldName = "< this component link >", FieldPath = "this_component_link", Link = true });
            }

            return res;
        }

        public static XElement GetDefaultXmlValue(this FieldMappingInfo mapping, IAlchemyCoreServiceClient client, XNamespace ns, string publicationId)
        {
            if (string.IsNullOrEmpty(mapping.DefaultValue))
                return null;

            object defaultValue = null;

            if (mapping.TargetField.Field.IsNumber())
            {
                defaultValue = (double?)double.Parse(mapping.DefaultValue);
            }
            else if (mapping.TargetField.Field.IsDate())
            {
                defaultValue = (DateTime?)DateTime.Parse(mapping.DefaultValue);
            }
            else
            {
                defaultValue = mapping.DefaultValue;
            }

            if (defaultValue.ToString().StartsWith("tcm:") && (mapping.TargetField.Field.IsComponentLink() || mapping.TargetField.Field.IsMultimedia() || mapping.TargetField.Field.IsKeyword()))
            {
                string localId = GetBluePrintItemTcmId(defaultValue.ToString(), publicationId);

                IdentifiableObjectData item = ReadItem(client, localId);
                if (item != null)
                {
                    if (mapping.TargetField.Field.IsComponentLink() || mapping.TargetField.Field.IsMultimedia())
                        return GetComponentLink(item.Id, item.Title, mapping.TargetField.Field.Name, ns);

                    if (mapping.TargetField.Field.IsKeyword())
                        return GetKeywordLink(item.Id, item.Title, mapping.TargetField.Field.Name, ns);
                }
            }

            if (mapping.TargetField.Field.GetFieldType() == FieldType.Xhtml)
            {
                XNamespace pNs = "http://www.w3.org/1999/xhtml";
                defaultValue = new XElement(pNs + "p", defaultValue);
            }

            return new XElement(ns + mapping.TargetField.Field.Name, defaultValue);
        }

        public static XElement GetByXPath(this XElement root, string xPath, XNamespace ns)
        {
            if (root == null || string.IsNullOrEmpty(xPath))
                return null;

            xPath = xPath.Trim('/');
            if (string.IsNullOrEmpty(xPath))
                return null;

            if (xPath.Contains("/"))
            {
                xPath = "/xhtml:" + xPath.Replace("/", "/xhtml:");
                XmlNamespaceManager namespaceManager = new XmlNamespaceManager(new NameTable());
                namespaceManager.AddNamespace("xhtml", ns.ToString());
                return root.XPathSelectElement(xPath, namespaceManager);
            }

            return root.Element(ns + xPath);
        }

        public static List<XElement> GetListByXPath(this XElement root, string xPath, XNamespace ns)
        {
            if (root == null || string.IsNullOrEmpty(xPath))
                return new List<XElement>();

            xPath = xPath.Trim('/');

            if (string.IsNullOrEmpty(xPath))
                return null;

            if (xPath.Contains("/"))
            {
                xPath = "/xhtml:" + xPath.Replace("/", "/xhtml:");
                XmlNamespaceManager namespaceManager = new XmlNamespaceManager(new NameTable());
                namespaceManager.AddNamespace("xhtml", ns.ToString());
                return root.XPathSelectElements(xPath, namespaceManager).ToList();
            }

            return root.Elements(ns + xPath).ToList();
        }

        public static List<XElement> GetListByXPath(this List<XElement> elements, string xPath, XNamespace ns)
        {
            return elements.SelectMany(x => x.GetListByXPath(xPath, ns)).ToList();
        }

        public static XElement Clone(this XElement sourceNode, string targetNodeName, XNamespace targetNs, bool stripFormatting = false)
        {
            if(stripFormatting)
                return new XElement(targetNs + targetNodeName, sourceNode.Value);

            if (sourceNode.GetDefaultNamespace() == "http://www.w3.org/1999/xhtml")
                return sourceNode;

            XNamespace linkNs = "http://www.w3.org/1999/xlink";
            if (sourceNode.Attribute(linkNs + "href") != null)
                return GetComponentLink(sourceNode.Attribute(linkNs + "href").Value, sourceNode.Attribute(linkNs + "title") == null ? null : sourceNode.Attribute(linkNs + "title").Value, targetNodeName, targetNs);

            if (sourceNode.Elements().Any())
                return new XElement(targetNs + targetNodeName, sourceNode.Attributes().Where(x => x.IsAllowed()), sourceNode.Elements().Select(x => x.Clone(x.Name.LocalName, targetNs)));

            return new XElement(targetNs + targetNodeName, sourceNode.Attributes().Where(x => x.IsAllowed()), sourceNode.Value);
        }

        public static bool IsAllowed(this XAttribute attr)
        {
            if (attr.IsNamespaceDeclaration)
            {
                if (attr.Value == "http://www.w3.org/1999/xhtml")
                    return true;
                return false;
            }

            return true;
        }

        public static XElement GetComponentLink(string id, string title, string fieldName, XNamespace ns)
        {
            XNamespace nsLocal = "http://www.w3.org/1999/xlink";

            if (string.IsNullOrEmpty(title))
                return new XElement(ns + fieldName,
                    new XAttribute(nsLocal + "href", id),
                    new XAttribute(XNamespace.Xmlns + "xlink", nsLocal));

            return new XElement(ns + fieldName,
                new XAttribute(nsLocal + "href", id),
                new XAttribute(nsLocal + "title", title),
                new XAttribute(XNamespace.Xmlns + "xlink", nsLocal));
        }

        public static XElement GetKeywordLink(string id, string title, string fieldName, XNamespace ns)
        {
            XElement res = GetComponentLink(id, title, fieldName, ns);
            res.Add(title);
            return res;
        }

        private static List<XElement> GetSourceMappedValues(IAlchemyCoreServiceClient client, FieldMappingInfo mapping, List<XElement> sourceElements, XNamespace sourceNs, ItemInfo sourceItem, string newTitle, XNamespace targetNs, string targetFolderUri, List<ResultInfo> results)
        {
            List<FieldMappingInfo> childFieldMapping = mapping.ChildFieldMapping != null ? mapping.ChildFieldMapping.Where(x => !string.IsNullOrEmpty(x.SourceFieldPath) || !string.IsNullOrEmpty(x.DefaultValue)).ToList() : null;

            //embedded node -> component link
            if (mapping.SourceField != null && mapping.SourceField.Field != null && mapping.SourceField.Field.IsEmbedded() && mapping.TargetField.Field.IsComponentLink() && childFieldMapping.Count > 0)
            {
                List<XElement> res = new List<XElement>();

                List<XElement> sourceNodes = sourceElements.GetListByXPath(mapping.SourceFieldPath, sourceNs);
                if (!mapping.TargetField.Field.IsMultiValue())
                    sourceNodes = sourceNodes.Take(1).ToList();

                int index = 0;
                foreach (XElement sourceElement in sourceNodes)
                {
                    XElement componentLink = EmbeddedSchemaToComponentLink(client, mapping, sourceElement, sourceNs, sourceItem, newTitle, (ComponentLinkFieldDefinitionData)mapping.TargetField.Field, targetFolderUri, index, results);
                    res.Add(componentLink);
                    index++;
                }
                return res;
            }

            //component link -> embedded node
            if (mapping.SourceField != null && mapping.SourceField.Field != null && mapping.SourceField.Field.IsComponentLink() && mapping.TargetField.Field.IsEmbedded() && childFieldMapping.Count > 0)
            {
                List<XElement> res = new List<XElement>();

                List<XElement> sourceCls = sourceElements.GetListByXPath(mapping.SourceFieldPath, sourceNs);
                if (!mapping.TargetField.Field.IsMultiValue())
                    sourceCls = sourceCls.Take(1).ToList();

                foreach (XElement sourceCl in sourceCls)
                {
                    XElement node = ComponentLinkToEmbeddedSchema(client, sourceCl, newTitle, (EmbeddedSchemaFieldDefinitionData)mapping.TargetField.Field, mapping.TargetFieldPath, mapping.ChildFieldMapping, targetFolderUri, results);
                    res.Add(node);
                }
                return res;
            }

            //embedded node -> embedded node (same embedded schema)
            if (mapping.SourceField != null && mapping.SourceField.Field != null && mapping.SourceField.Field.IsEmbedded() && mapping.TargetField.Field.IsEmbedded() && ((EmbeddedSchemaFieldDefinitionData)mapping.SourceField.Field).EmbeddedSchema.IdRef == ((EmbeddedSchemaFieldDefinitionData)mapping.TargetField.Field).EmbeddedSchema.IdRef && !AnyDefaultValue(childFieldMapping))
            {
                List<XElement> res = new List<XElement>();

                List<XElement> sourceNodes = sourceElements.GetListByXPath(mapping.SourceFieldPath, sourceNs);
                if (!mapping.TargetField.Field.IsMultiValue())
                    sourceNodes = sourceNodes.Take(1).ToList();

                foreach (XElement sourceNode in sourceNodes)
                {
                    XElement node = sourceNode.Clone(mapping.TargetField.Field.Name, targetNs);
                    res.Add(node);
                }

                return res;
            }

            //embedded node -> embedded node
            if (mapping.TargetField.Field.IsEmbedded() && childFieldMapping != null && childFieldMapping.Count > 0)
            {
                List<XElement> res = new List<XElement>();

                int maxCount = childFieldMapping.Max(x => sourceElements.GetListByXPath(x.SourceFieldPath, sourceNs).Count);
                if (!mapping.TargetField.Field.IsMultiValue() && maxCount > 1)
                    maxCount = 1;

                //if (maxCount > 0 && mapping.TargetField.Field.IsMultiValue())
                //{
                //    for (int i = 0; i < maxCount; i++)
                //    {
                //        res.Add(new XElement(targetNs + mapping.TargetField.Field.Name));
                //    }
                //}
                //else
                if (maxCount > 0 || AnyDefaultValue(childFieldMapping))
                {
                    res.Add(new XElement(targetNs + mapping.TargetField.Field.Name));
                }

                foreach (FieldMappingInfo childMapping in childFieldMapping)
                {
                    List<XElement> children = GetSourceMappedValues(client, childMapping, sourceElements, sourceNs, sourceItem, newTitle, targetNs, targetFolderUri, results);

                    if (children != null && children.Any())
                    {
                        //move to the upper level
                        //if (mapping.TargetField.Field.IsMultiValue() && !childMapping.TargetField.Field.IsMultiValue())
                        //{
                        //    int i = 0;
                        //    foreach (XElement child in children)
                        //    {
                        //        res[i].Add(child);
                        //        i++;
                        //    }
                        //}
                        //multi to multi
                        //else 
                        if (childMapping.TargetField.Field.IsMultiValue())
                        {
                            foreach (XElement child in children)
                            {
                                res[0].Add(child);
                            }
                        }
                        //to single
                        else
                        {
                            res[0].Add(children.First());
                        }
                    }
                }

                return res;
            }

            //process primitive, component link -> component link
            else
            {
                List<XElement> res = new List<XElement>();

                XElement defaultValue = mapping.GetDefaultXmlValue(client, targetNs, GetPublicationTcmId(targetFolderUri));

                if (mapping.SourceFieldPath == "create_new")
                {
                    res = defaultValue != null ? new List<XElement> { defaultValue } : new List<XElement>();
                }
                else
                {
                    res = sourceElements.GetListByXPath(mapping.SourceFieldPath, sourceNs);
                    if (!mapping.TargetField.Field.IsMultiValue())
                        res = res.Take(1).ToList();

                    if (res != null && res.Count > 0)
                    {
                        bool stripHtml = mapping.SourceField != null && mapping.TargetField != null &&
                            mapping.SourceField.Field.GetFieldType() == FieldType.Xhtml && mapping.TargetField.Field.GetFieldType() != FieldType.Xhtml;

                        res = res.Select(x => x.Clone(mapping.TargetField.Field.Name, targetNs, stripHtml)).ToList();
                    }
                    else
                    {
                        res = defaultValue != null ? new List<XElement> { defaultValue } : new List<XElement>();
                    }
                }

                return res;
            }
        }

        public static XElement EmbeddedSchemaToComponentLink(IAlchemyCoreServiceClient client, FieldMappingInfo mapping, XElement sourceElement, XNamespace sourceNs, ItemInfo sourceItem, string newTitle, ComponentLinkFieldDefinitionData targetField, string targetFolderUri, int index, List<ResultInfo> results)
        {
            if (sourceElement == null || targetField == null || mapping == null || mapping.ChildFieldMapping == null)
                return null;

            if (!targetField.AllowedTargetSchemas.Any())
                return null;

            string targetSchemaUri = targetField.AllowedTargetSchemas[0].IdRef;

            SchemaData targetSchema = ReadItem(client, targetSchemaUri) as SchemaData;
            if (targetSchema == null)
                return null;

            List<ItemFieldDefinitionData> targetFields = GetSchemaFields(client, targetSchemaUri);

            XElement fixedNode = GetFixedXml(client, new List<XElement> { sourceElement }, sourceNs, sourceItem, newTitle, targetSchema.NamespaceUri, targetSchema.RootElementName, targetFields, targetFolderUri, mapping.ChildFieldMapping, results);
            if (fixedNode == null)
                return null;

            string newXml = fixedNode.ToString();

            //replace to local publication ids
            newXml = newXml.GetLocalizedXml(targetFolderUri);

            string title = string.Format("[{0:00}0] {1}", index, newTitle);

            ResultInfo result = SaveComponent(client, null, null, null, targetSchema, title, newXml, string.Empty, targetFolderUri, false);
            results.Add(result);

            return GetComponentLink(result.Item.TcmId, title, targetField.Name, targetSchema.NamespaceUri);
        }

        public static XElement ComponentLinkToEmbeddedSchema(IAlchemyCoreServiceClient client, XElement sourceElement, string newTitle, EmbeddedSchemaFieldDefinitionData targetField, string targetRoot, List<FieldMappingInfo> childFieldMapping, string targetFolderUri, List<ResultInfo> results)
        {
            XNamespace ns = "http://www.w3.org/1999/xlink";
            string sourceComponentUri = sourceElement.Attribute(ns + "href").Value;

            ComponentData sourceComponent = client.Read(sourceComponentUri, new ReadOptions()) as ComponentData;
            if (sourceComponent == null || string.IsNullOrEmpty(sourceComponent.Content))
                return null;

            string sourceSchemaUri = sourceComponent.Schema.IdRef;
            SchemaData sourceSchema = client.Read(sourceSchemaUri, null) as SchemaData;
            if (sourceSchema == null)
                return null;

            string targetEmbeddedSchemaUri = targetField.EmbeddedSchema.IdRef;
            SchemaData targetEmbeddedSchema = client.Read(targetEmbeddedSchemaUri, null) as SchemaData;
            if (targetEmbeddedSchema == null)
                return null;

            List<ItemFieldDefinitionData> targetEmbeddedSchemaFields = GetSchemaFields(client, targetEmbeddedSchemaUri);

            XDocument doc = XDocument.Parse(sourceComponent.Content);
            if (doc.Root == null)
                return null;

            XElement fixedXml = GetFixedXml(client, new List<XElement> { doc.Root }, sourceSchema.NamespaceUri, sourceComponent.ToItem(), newTitle, targetEmbeddedSchema.NamespaceUri, targetRoot, targetEmbeddedSchemaFields, targetFolderUri, childFieldMapping, results);

            return fixedXml;
        }

        public static XElement GetFixedXml(IAlchemyCoreServiceClient client, List<XElement> sourceElements, XNamespace sourceNs, ItemInfo sourceItem, string newTitle, XNamespace targetNs, string targetRoot, List<ItemFieldDefinitionData> targetFields, string targetFolderUri, List<FieldMappingInfo> fieldMapping, List<ResultInfo> results)
        {
            if (sourceElements == null || sourceElements.Count == 0)
                return null;

            string targetName = targetRoot.Contains("/") ? targetRoot.Split('/').Last() : targetRoot;

            XElement res = new XElement(targetNs + targetName);

            foreach (ItemFieldDefinitionData targetField in targetFields)
            {
                FieldMappingInfo mapping = fieldMapping.FirstOrDefault(x => x.TargetField != null && x.TargetField.FieldPath == targetRoot + "/" + targetField.Name);
                if (mapping == null)
                    continue;

                //construct Component Link to the source component
                if (mapping.SourceFieldPath == "this_component_link")
                {
                    ComponentData component = GetComponent(client, sourceItem.TcmId);
                    XElement item = GetComponentLink(sourceItem.TcmId, component.Title, targetField.Name, targetNs);
                    res.Add(item);
                }

                //construct primitive or new Embedded Schema
                else
                {
                    List<XElement> targetElements = GetSourceMappedValues(client, mapping, sourceElements, sourceNs, sourceItem, newTitle, targetNs, targetFolderUri, results);
                    foreach (XElement targetElement in targetElements)
                    {
                        res.Add(targetElement);
                    }
                }
            }

            return res;
        }

        private static string GetFixedContent(IAlchemyCoreServiceClient client, List<string> xmlSources, XNamespace sourceNs, ItemInfo sourceItem, string newTitle, SchemaData targetSchema, string targetRoot, List<ItemFieldDefinitionData> targetFields, string targetFolderUri, List<FieldMappingInfo> fieldMapping, List<ResultInfo> results)
        {
            if (targetFields == null || targetFields.Count == 0)
                return string.Empty;

            List<XElement> sources = new List<XElement>();
            foreach (string sourceXml in xmlSources)
            {
                if (string.IsNullOrEmpty(sourceXml))
                    continue;

                XDocument doc = XDocument.Parse(sourceXml);
                sources.Add(doc.Root);
            }

            if(sources.Count == 0)
                return string.Empty;

            if (fieldMapping == null)
                fieldMapping = GetDefaultFieldMapping(client, targetSchema, targetRoot == "Metadata" ? null : targetFields, targetRoot == "Metadata" ? targetFields : null)
                    .GetNestedFieldMapping(targetRoot == "Metadata" ? "m.0" : "0", 0);

            if (results == null)
                results = new List<ResultInfo>();

            XElement fixedXml = GetFixedXml(client, sources, sourceNs, sourceItem, newTitle, targetSchema.NamespaceUri, targetRoot, targetFields, targetFolderUri, fieldMapping, results);

            string res = fixedXml.ToString();

            //replace to local publication ids
            res = res.GetLocalizedXml(targetFolderUri);

            return res;
        }

        private static ResultInfo SaveComponent(IAlchemyCoreServiceClient client, IAlchemyStreamDownload downloadClient, IAlchemyStreamUpload uploadClient, ComponentData sourceComponent, SchemaData schema, string title, string contentXml, string metadataXml, string folderUri, bool localize)
        {
            ResultInfo result = new ResultInfo();

            if (string.IsNullOrEmpty(title))
            {
                result.Status = Status.Error;
                result.Message = "Component title is not defined";

                return result;
            }

            //check existing item
            List<ItemInfo> targetFolderItems = GetItemsByParentContainer(client, folderUri);
            if (targetFolderItems.All(x => x.Title != title))
            {
                try
                {
                    //create new component

                    ComponentData component = new ComponentData
                    {
                        Title = title,
                        LocationInfo = new LocationInfo { OrganizationalItem = new LinkToOrganizationalItemData { IdRef = folderUri } },
                        Id = "tcm:0-0-0",
                        Schema = new LinkToSchemaData { IdRef = schema.Id },
                        Content = contentXml,
                        Metadata = metadataXml,
                        IsBasedOnMandatorySchema = false,
                        IsBasedOnTridionWebSchema = true,
                        ApprovalStatus = new LinkToApprovalStatusData { IdRef = "tcm:0-0-0" }
                    };

                    if (schema.Purpose == SchemaPurpose.Multimedia && sourceComponent != null && sourceComponent.BinaryContent != null && downloadClient!= null && uploadClient != null)
                    {
                        //set multimedia component properties

                        component.Content = string.Empty;

                        string origFilename = sourceComponent.BinaryContent.Filename;
                        string extension = origFilename.Substring(origFilename.LastIndexOf('.') + 1);
                        string tempPath = string.Empty;

                        // Upload binary content to cms 
                        using (var tempStream = downloadClient.DownloadBinaryContent(sourceComponent.Id))
                        {
                            // Uses the content stream from original component
                            AccessTokenData accessToken = client.GetCurrentUserWithToken();
                            tempPath = uploadClient.UploadBinaryContent(accessToken, tempStream);
                        }

                        // Find multimedia type
                        var list = client.GetSystemWideList(new MultimediaTypesFilterData());
                        var multimediaType = list.OfType<MultimediaTypeData>().Single(mt => mt.FileExtensions.Contains(extension));

                        // Set binary content of component
                        component.BinaryContent = new BinaryContentData
                        {
                            UploadFromFile = tempPath,
                            Filename = origFilename,
                            MultimediaType = new LinkToMultimediaTypeData { IdRef = multimediaType.Id }
                        };
                    }

                    component = (ComponentData)client.Create(component, new ReadOptions());

                    string componentUri = component.Id;
                    component = GetComponent(client, componentUri);

                    result.Item = component.ToItem();
                    result.Status = Status.Success;
                    result.Message = string.Format("Component created: {0}", component.GetWebDav());
                }
                catch (Exception ex)
                {
                    result.Status = Status.Error;
                    result.Item = new ItemInfo { ItemType = ItemType.Component };
                    result.ErrorMessage = ex.Message;
                    result.Message = string.Format("Error creating component: {0}", title);
                }
            }
            else
            {
                //update existing component
                string componentUri = targetFolderItems.First(x => x.Title == title).TcmId;

                ComponentData component = GetComponent(client, componentUri);

                //only component of same name and title
                if (component != null && component.Schema.IdRef.GetId() == schema.Id.GetId())
                {
                    if ((component.Content.PrettyXml() == contentXml.PrettyXml() && component.Metadata.PrettyXml() == metadataXml.PrettyXml()))
                    {
                        result.Item = component.ToItem();
                        result.Status = Status.Info;
                        result.Message = "Component was not changed";

                        return result;
                    }

                    //localize if item is shared
                    if (component.BluePrintInfo.IsShared == true)
                    {
                        if (localize)
                        {
                            Localize(client, component.ToItem());
                        }
                        else
                        {
                            componentUri = GetBluePrintLocalizedTcmId(component);
                            component = GetComponent(client, componentUri);
                        }
                    }

                    try
                    {
                        component = client.CheckOut(component.Id, true, new ReadOptions()) as ComponentData;
                    }
                    catch (Exception ex)
                    {
                    }

                    try
                    {
                        component.Content = contentXml;
                        component.Metadata = metadataXml;

                        client.Update(component, new ReadOptions());

                        client.CheckIn(component.Id, true, "Updated by Component Transformer", new ReadOptions());

                        result.Item = component.ToItem();
                        result.Status = Status.Success;
                        result.Message = string.Format("Updated component: {0}", component.GetWebDav());
                    }
                    catch (Exception ex)
                    {
                        client.UndoCheckOut(componentUri, true, new ReadOptions());

                        result.Status = Status.Error;
                        result.Item = new ItemInfo { ItemType = ItemType.Component, TcmId = componentUri };
                        result.ErrorMessage = ex.Message;
                        result.Message = string.Format("Error updating component: {0}", component.GetWebDav());
                    }
                }
                else
                {
                    result.Status = Status.Error;
                    result.Item = new ItemInfo { ItemType = ItemType.Component, TcmId = componentUri };
                    result.Message = string.Format("Error updating component \"{0}\"", title);
                }
            }

            return result;
        }

        private static ResultInfo SaveTridionObjectMetadata(IAlchemyCoreServiceClient client, SchemaData metadataSchema, string title, string metadataXml, string containerUri, bool localize)
        {
            ResultInfo result = new ResultInfo();

            //check existing item
            List<ItemInfo> targetContainerItems = GetItemsByParentContainer(client, containerUri);
            if (targetContainerItems.All(x => x.Title != title))
            {
                result.Status = Status.Error;
                result.Message = "Item not found";

                return result;
            }

            //update existing tridionObject
            string tridionObjectUri = targetContainerItems.First(x => x.Title == title).TcmId;

            RepositoryLocalObjectData tridionObject = ReadItem(client, tridionObjectUri) as RepositoryLocalObjectData;

            //only tridionObject of same name and title
            if (tridionObject != null && tridionObject.MetadataSchema.IdRef.GetId() == metadataSchema.Id.GetId())
            {
                if ((tridionObject.Metadata.PrettyXml() == metadataXml.PrettyXml()))
                {
                    result.Item = tridionObject.ToItem();
                    result.Status = Status.Info;
                    result.Message = "Item was not changed";

                    return result;
                }

                //localize if item is shared
                if (tridionObject.BluePrintInfo.IsShared == true)
                {
                    if (localize)
                    {
                        Localize(client, tridionObject.ToItem());
                    }
                    else
                    {
                        tridionObjectUri = GetBluePrintLocalizedTcmId(tridionObject);
                        tridionObject = ReadItem(client, tridionObjectUri) as RepositoryLocalObjectData;
                    }
                }

                try
                {
                    tridionObject = client.CheckOut(tridionObject.Id, true, new ReadOptions());
                }
                catch (Exception ex)
                {
                }

                try
                {
                    tridionObject.Metadata = metadataXml;

                    client.Update(tridionObject, new ReadOptions());

                    client.CheckIn(tridionObject.Id, true, "Updated by Component Transformer", new ReadOptions());

                    result.Item = tridionObject.ToItem();
                    result.Status = Status.Success;
                    result.Message = string.Format("Updated item: {0}", tridionObject.GetWebDav());
                }
                catch (Exception ex)
                {
                    client.UndoCheckOut(tridionObjectUri, true, new ReadOptions());

                    result.Status = Status.Error;
                    result.Item = new ItemInfo { ItemType = GetItemType(tridionObjectUri), TcmId = tridionObjectUri };
                    result.ErrorMessage = ex.Message;
                    result.Message = string.Format("Error updating item: {0}", tridionObject.GetWebDav());
                }
            }
            else
            {
                result.Status = Status.Error;
                result.Item = new ItemInfo { ItemType = GetItemType(tridionObjectUri), TcmId = tridionObjectUri };
                result.Message = string.Format("Error updating page: {0}", title);
            }

            return result;
        }

        public static void ChangeSchemaForComponent(IAlchemyCoreServiceClient client, string componentUri, string sourceSchemaUri, string targetSchemaUri, string targetFolderUri, bool localize, List<FieldMappingInfo> fieldMapping, List<FieldMappingInfo> metadataFieldMapping, List<ResultInfo> results)
        {
            // Open up the source component schema 
            SchemaData sourceSchema = client.Read(sourceSchemaUri, null) as SchemaData;
            if (sourceSchema == null)
                return;

            // Open up the target component schema
            SchemaData targetSchema = client.Read(targetSchemaUri, null) as SchemaData;
            if (targetSchema == null)
                return;

            List<ItemFieldDefinitionData> targetComponentFields = GetSchemaFields(client, targetSchemaUri);
            List<ItemFieldDefinitionData> targetMetadataFields = GetSchemaMetadataFields(client, targetSchemaUri);

            // Change schema for component
            ChangeSchemaForComponent(client, componentUri, sourceSchema, targetSchema, targetComponentFields, targetMetadataFields, targetFolderUri, localize, fieldMapping, metadataFieldMapping, results);
        }

        private static void ChangeSchemaForComponent(IAlchemyCoreServiceClient client, string componentUri, SchemaData sourceSchema, SchemaData targetSchema, List<ItemFieldDefinitionData> targetComponentFields, List<ItemFieldDefinitionData> targetMetadataFields, string targetFolderUri, bool localize, List<FieldMappingInfo> fieldMapping, List<FieldMappingInfo> metadataFieldMapping, List<ResultInfo> results)
        {
            if (string.IsNullOrEmpty(componentUri))
                return;

            if (results == null)
                results = new List<ResultInfo>();

            ComponentData component = GetComponent(client, componentUri);
            if (component == null)
                return;

            if (!component.Schema.IdRef.GetId().Equals(sourceSchema.Id.GetId()))
            {
                // If the component is not of the schmea that we want to change from, do nothing...
                return;
            }

            if (component.Schema.IdRef.GetId().Equals(targetSchema.Id.GetId()))
            {
                // If the component already has this schema, don't do anything.
                return;
            }

            //localize if item is shared
            if (component.BluePrintInfo.IsShared == true)
            {
                if (localize)
                {
                    Localize(client, component.ToItem());
                }
                else
                {
                    componentUri = GetBluePrintLocalizedTcmId(component);
                    component = GetComponent(client, componentUri);
                    targetFolderUri = GetBluePrintItemTcmId(targetFolderUri, GetPublicationTcmId(componentUri));
                }
            }

            ResultInfo result = new ResultInfo();

            //get fixed xml
            string newContent = GetFixedContent(client, new List<string> { component.Content, component.Metadata }, sourceSchema.NamespaceUri, component.ToItem(), component.Title, targetSchema, targetSchema.RootElementName, targetComponentFields, targetFolderUri, fieldMapping, results);

            //get fixed metadata
            string newMetadata = GetFixedContent(client, new List<string> { component.Content, component.Metadata }, sourceSchema.NamespaceUri, component.ToItem(), component.Title, targetSchema, "Metadata", targetMetadataFields, targetFolderUri, metadataFieldMapping, results);

            if (string.IsNullOrEmpty(newContent) && string.IsNullOrEmpty(newMetadata))
                return;

            try
            {
                component = client.CheckOut(component.Id, true, new ReadOptions()) as ComponentData;
            }
            catch (Exception ex)
            {
            }

            try
            {
                //rebild component xml
                component.Content = newContent;

                //rebuild metadata
                component.Metadata = newMetadata;

                //change schema id
                component.Schema.IdRef = GetBluePrintItemTcmId(targetSchema.Id, GetPublicationTcmId(componentUri));

                client.Update(component, new ReadOptions());

                client.CheckIn(component.Id, true, "Updated by Component Transformer", new ReadOptions());

                result.Item = component.ToItem();
                result.Status = Status.Success;
                result.Message = string.Format("Changed used schema: {0}", component.GetWebDav());
            }
            catch (Exception ex)
            {
                client.UndoCheckOut(componentUri, true, new ReadOptions());

                result.Status = Status.Error;
                result.Item = new ItemInfo { ItemType = ItemType.Component, TcmId = componentUri };
                result.ErrorMessage = ex.Message;
                result.Message = string.Format("Error changing schema: {0}", component.GetWebDav());
            }

            results.Add(result);
        }

        public static void FixComponent(IAlchemyCoreServiceClient client, string componentUri, string schemaUri, string targetFolderUri, bool localize, List<FieldMappingInfo> fieldMapping, List<FieldMappingInfo> metadataFieldMapping, List<ResultInfo> results)
        {
            SchemaData schema = client.Read(schemaUri, null) as SchemaData;
            if (schema == null)
                return;

            List<ItemFieldDefinitionData> componentFields = GetSchemaFields(client, schemaUri);
            List<ItemFieldDefinitionData> metadataFields = GetSchemaMetadataFields(client, schemaUri);

            // Fix component
            FixComponent(client, componentUri, schema, componentFields, metadataFields, targetFolderUri, localize, fieldMapping, metadataFieldMapping, results);
        }

        private static void FixComponent(IAlchemyCoreServiceClient client, string componentUri, SchemaData schema, List<ItemFieldDefinitionData> componentFields, List<ItemFieldDefinitionData> metadataFields, string targetFolderUri, bool localize, List<FieldMappingInfo> fieldMapping, List<FieldMappingInfo> metadataFieldMapping, List<ResultInfo> results)
        {
            if (string.IsNullOrEmpty(componentUri))
                return;

            if (results == null)
                results = new List<ResultInfo>();

            ComponentData component = GetComponent(client, componentUri);
            if (component == null)
                return;

            if (!component.Schema.IdRef.GetId().Equals(schema.Id.GetId()))
            {
                // If the component is not of the schema, do nothing...
                return;
            }

            //localize if item is shared
            if (component.BluePrintInfo.IsShared == true)
            {
                if (localize)
                {
                    Localize(client, component.ToItem());
                }
                else
                {
                    componentUri = GetBluePrintLocalizedTcmId(component);
                    component = GetComponent(client, componentUri);
                    targetFolderUri = GetBluePrintItemTcmId(targetFolderUri, GetPublicationTcmId(componentUri));
                }
            }

            //get fixed xml
            string newContent = GetFixedContent(client, new List<string> { component.Content, component.Metadata }, schema.NamespaceUri, component.ToItem(), component.Title, schema, schema.RootElementName, componentFields, targetFolderUri, fieldMapping, results);

            //get fixed metadata
            string newMetadata = GetFixedContent(client, new List<string> { component.Content, component.Metadata }, schema.NamespaceUri, component.ToItem(), component.Title, schema, "Metadata", metadataFields, targetFolderUri, metadataFieldMapping, results);

            if (string.IsNullOrEmpty(newContent) && string.IsNullOrEmpty(newMetadata))
                return;

            ResultInfo result = SaveComponent(client, null, null, null, schema, component.Title, newContent, newMetadata, component.LocationInfo.OrganizationalItem.IdRef, localize);
            results.Add(result);
        }

        public static void TransformComponent(IAlchemyCoreServiceClient client, IAlchemyStreamDownload downloadClient, IAlchemyStreamUpload uploadClient, string sourceComponentUri, string sourceFolderUri, string sourceSchemaUri, string targetFolderUri, string targetSchemaUri, string newTitle, bool localize, List<FieldMappingInfo> fieldMapping, List<FieldMappingInfo> metadataFieldMapping, TargetLinkInfo targetLink, List<ResultInfo> results)
        {
            if (string.IsNullOrEmpty(sourceComponentUri))
                return;

            SchemaData sourceSchema = client.Read(sourceSchemaUri, null) as SchemaData;
            if (sourceSchema == null)
                return;

            List<ItemFieldDefinitionData> sourceComponentFields = GetSchemaFields(client, sourceSchemaUri);
            List<ItemFieldDefinitionData> sourceMetadataFields = GetSchemaMetadataFields(client, sourceSchemaUri);

            SchemaData targetSchema = client.Read(targetSchemaUri, null) as SchemaData;
            if (targetSchema == null)
                return;

            List<ItemFieldDefinitionData> targetComponentFields = GetSchemaFields(client, targetSchemaUri);
            List<ItemFieldDefinitionData> targetMetadataFields = GetSchemaMetadataFields(client, targetSchemaUri);

            // Change schema for component
            TransformComponent(client, downloadClient, uploadClient, sourceComponentUri, sourceFolderUri, sourceSchema, sourceComponentFields, sourceMetadataFields, targetFolderUri, targetSchema, targetComponentFields, targetMetadataFields, newTitle, localize, fieldMapping, metadataFieldMapping, targetLink, results);
        }

        private static void TransformComponent(IAlchemyCoreServiceClient client, IAlchemyStreamDownload downloadClient, IAlchemyStreamUpload uploadClient, string sourceComponentUri, string sourceFolderUri, SchemaData sourceSchema, List<ItemFieldDefinitionData> sourceComponentFields, List<ItemFieldDefinitionData> sourceMetadataFields, string targetFolderUri, SchemaData targetSchema, List<ItemFieldDefinitionData> targetComponentFields, List<ItemFieldDefinitionData> targetMetadataFields, string newTitle, bool localize, List<FieldMappingInfo> fieldMapping, List<FieldMappingInfo> metadataFieldMapping, TargetLinkInfo targetLink, List<ResultInfo> results)
        {
            if (string.IsNullOrEmpty(sourceComponentUri))
                return;

            if (results == null)
                results = new List<ResultInfo>();

            ComponentData component = GetComponent(client, sourceComponentUri);
            if (component == null || string.IsNullOrEmpty(component.Content) && string.IsNullOrEmpty(component.Metadata))
                return;

            if (!component.Schema.IdRef.GetId().Equals(sourceSchema.Id.GetId()))
            {
                // If the component is not of the schema, do nothing...
                return;
            }

            // create folder chain
            if (!string.IsNullOrEmpty(sourceFolderUri))
            {
                FolderData sourceFolder = client.Read(sourceFolderUri, new ReadOptions()) as FolderData;
                if (sourceFolder != null)
                {
                    List<string> componentWebDavChain = component.GetWebDavChain();
                    List<string> folderChain = componentWebDavChain.Take(componentWebDavChain.Count - 1).ToList().Substract(sourceFolder.GetWebDavChain());
                    targetFolderUri = CreateFolderChain(client, folderChain, targetFolderUri);
                }
            }

            //get fixed xml
            string newContent = GetFixedContent(client, new List<string> { component.Content, component.Metadata }, sourceSchema.NamespaceUri, component.ToItem(), newTitle, targetSchema, targetSchema.RootElementName, targetComponentFields, targetFolderUri, fieldMapping, results);

            //get fixed metadata
            string newMetadata = GetFixedContent(client, new List<string> { component.Content, component.Metadata }, sourceSchema.NamespaceUri, component.ToItem(), newTitle, targetSchema, "Metadata", targetMetadataFields, targetFolderUri, metadataFieldMapping, results);

            if (string.IsNullOrEmpty(newContent) && string.IsNullOrEmpty(newMetadata))
                return;

            ResultInfo result = SaveComponent(client, downloadClient, uploadClient, component, targetSchema, newTitle, newContent, newMetadata, targetFolderUri, localize);
            results.Add(result);

            //save component link to target component to source component fields, if "target component link" selected

            if (result.Status == Status.Success && !string.IsNullOrEmpty(result.Item.TcmId) && targetLink != null && !string.IsNullOrEmpty(targetLink.Path))
            {
                var newFieldMapping = GetDefaultFieldMapping(client, sourceSchema, sourceComponentFields, null);
                foreach (FieldMappingInfo newMapping in newFieldMapping)
                {
                    if (newMapping.TargetFieldPath == targetLink.Path)
                    {
                        newMapping.DefaultValue = result.Item.TcmId;
                        if(targetLink.Overwrite)
                            newMapping.SourceFieldPath = "create_new";
                    }
                }
                newFieldMapping = newFieldMapping.GetNestedFieldMapping("0", 0);

                var newMetadataMapping = GetDefaultFieldMapping(client, sourceSchema, null, sourceMetadataFields);
                foreach (FieldMappingInfo newMapping in newMetadataMapping)
                {
                    if (newMapping.TargetFieldPath == targetLink.Path)
                    {
                        newMapping.DefaultValue = result.Item.TcmId;
                        if (targetLink.Overwrite)
                            newMapping.SourceFieldPath = "create_new";
                    }
                }
                newMetadataMapping = newMetadataMapping.GetNestedFieldMapping("m.0", 0);

                FixComponent(client, sourceComponentUri, sourceSchema, sourceComponentFields, sourceMetadataFields, targetFolderUri, localize, newFieldMapping, newMetadataMapping, results);
            }
        }

        public static void ChangeMetadataSchemaForTridionObject(IAlchemyCoreServiceClient client, string sourceTridionObjectUri, string sourceMetadataSchemaUri, string targetContainerUri, string targetMetadataSchemaUri, bool localize, List<FieldMappingInfo> metadataFieldMapping, List<ResultInfo> results)
        {
            // Open up the source metadata schema 
            SchemaData sourceMetadataSchema = client.Read(sourceMetadataSchemaUri, null) as SchemaData;
            if (sourceMetadataSchema == null)
                return;

            List<ItemFieldDefinitionData> sourceMetadataFields = GetSchemaMetadataFields(client, sourceMetadataSchemaUri);

            // Open up the target metadata schema
            SchemaData targetMetadataSchema = client.Read(targetMetadataSchemaUri, null) as SchemaData;
            if (targetMetadataSchema == null)
                return;

            List<ItemFieldDefinitionData> targetMetadataFields = GetSchemaMetadataFields(client, targetMetadataSchemaUri);

            // Change schema for tridion object
            ChangeMetadataSchemaForTridionObject(client, sourceTridionObjectUri, sourceMetadataSchema, sourceMetadataFields, targetContainerUri, targetMetadataSchema, targetMetadataFields, localize, metadataFieldMapping, results);
        }

        private static void ChangeMetadataSchemaForTridionObject(IAlchemyCoreServiceClient client, string sourceTridionObjectUri, SchemaData sourceMetadataSchema, List<ItemFieldDefinitionData> sourceMetadataFields, string targetContainerUri, SchemaData targetMetadataSchema, List<ItemFieldDefinitionData> targetMetadataFields, bool localize, List<FieldMappingInfo> metadataFieldMapping, List<ResultInfo> results)
        {
            if (string.IsNullOrEmpty(sourceTridionObjectUri))
                return;

            if (results == null)
                results = new List<ResultInfo>();

            RepositoryLocalObjectData tridionObject = ReadItem(client, sourceTridionObjectUri) as RepositoryLocalObjectData;
            if (tridionObject == null)
                return;

            if (!tridionObject.MetadataSchema.IdRef.GetId().Equals(sourceMetadataSchema.Id.GetId()))
            {
                // If the object is not of the metadata schema that we want to change from, do nothing...
                return;
            }

            if (tridionObject.MetadataSchema.IdRef.GetId().Equals(targetMetadataSchema.Id.GetId()))
            {
                // If the object already has this metadata schema, don't do anything.
                return;
            }

            //localize if item is shared
            if (tridionObject.BluePrintInfo.IsShared == true)
            {
                if (localize)
                {
                    Localize(client, tridionObject.ToItem());
                }
                else
                {
                    sourceTridionObjectUri = GetBluePrintLocalizedTcmId(tridionObject);
                    tridionObject = ReadItem(client, sourceTridionObjectUri) as RepositoryLocalObjectData;
                    targetContainerUri = GetBluePrintItemTcmId(targetContainerUri, GetPublicationTcmId(sourceTridionObjectUri));
                }
            }

            ResultInfo result = new ResultInfo();

            //get fixed metadata
            string newMetadata = GetFixedContent(client, new List<string> { tridionObject.Metadata }, sourceMetadataSchema.NamespaceUri, tridionObject.ToItem(), tridionObject.Title, targetMetadataSchema, "Metadata", targetMetadataFields, targetContainerUri, metadataFieldMapping, results);

            if (string.IsNullOrEmpty(newMetadata))
                return;

            try
            {
                tridionObject = client.CheckOut(tridionObject.Id, true, new ReadOptions());
            }
            catch (Exception ex)
            {
            }

            try
            {
                //rebuild metadata
                tridionObject.Metadata = newMetadata;

                //change schema id
                tridionObject.MetadataSchema.IdRef = GetBluePrintItemTcmId(targetMetadataSchema.Id, GetPublicationTcmId(sourceTridionObjectUri));

                client.Update(tridionObject, new ReadOptions());

                client.CheckIn(tridionObject.Id, true, "Updated by Component Transformer", new ReadOptions());

                result.Item = tridionObject.ToItem();
                result.Status = Status.Success;
                result.Message = string.Format("Changed metadata schema: {0}", tridionObject.GetWebDav());
            }
            catch (Exception ex)
            {
                client.UndoCheckOut(sourceTridionObjectUri, true, new ReadOptions());

                result.Status = Status.Error;
                result.Item = new ItemInfo { ItemType = GetItemType(sourceTridionObjectUri), TcmId = sourceTridionObjectUri };
                result.ErrorMessage = ex.Message;
                result.Message = string.Format("Error changing metadata schema: {0}", tridionObject.GetWebDav());
            }

            results.Add(result);
        }

        public static void FixTridionObjectMetadata(IAlchemyCoreServiceClient client, string sourceTridionObjectUri, string sourceMetadataSchemaUri, string targetContainerUri, bool localize, List<FieldMappingInfo> metadataFieldMapping, List<ResultInfo> results)
        {
            // Open up the schema
            SchemaData metadataSchema = client.Read(sourceMetadataSchemaUri, null) as SchemaData;
            if (metadataSchema == null)
                return;

            List<ItemFieldDefinitionData> metadataFields = GetSchemaMetadataFields(client, sourceMetadataSchemaUri);

            // Fix metadata for tridion object
            FixTridionObjectMetadata(client, sourceTridionObjectUri, metadataSchema, metadataFields, targetContainerUri, localize, metadataFieldMapping, results);
        }

        private static void FixTridionObjectMetadata(IAlchemyCoreServiceClient client, string sourceTridionObjectUri, SchemaData sourceMetadataSchema, List<ItemFieldDefinitionData> sourceMetadataFields, string targetContainerUri, bool localize, List<FieldMappingInfo> metadataFieldMapping, List<ResultInfo> results)
        {
            if (string.IsNullOrEmpty(sourceTridionObjectUri))
                return;

            if (results == null)
                results = new List<ResultInfo>();

            RepositoryLocalObjectData tridionObject = ReadItem(client, sourceTridionObjectUri) as RepositoryLocalObjectData;
            if (tridionObject == null || string.IsNullOrEmpty(tridionObject.Metadata))
                return;

            if (!tridionObject.MetadataSchema.IdRef.GetId().Equals(sourceMetadataSchema.Id.GetId()))
            {
                // If the tridion object is not of the metadata schema, do nothing...
                return;
            }

            //localize if item is shared
            if (tridionObject.BluePrintInfo.IsShared == true)
            {
                if (localize)
                {
                    Localize(client, tridionObject.ToItem());
                }
                else
                {
                    sourceTridionObjectUri = GetBluePrintLocalizedTcmId(tridionObject);
                    tridionObject = ReadItem(client, sourceTridionObjectUri) as RepositoryLocalObjectData;
                    targetContainerUri = GetBluePrintItemTcmId(targetContainerUri, GetPublicationTcmId(sourceTridionObjectUri));
                }
            }

            //get fixed metadata
            string newMetadata = GetFixedContent(client, new List<string> { tridionObject.Metadata }, sourceMetadataSchema.NamespaceUri, tridionObject.ToItem(), tridionObject.Title, sourceMetadataSchema, "Metadata", sourceMetadataFields, targetContainerUri, metadataFieldMapping, results);

            if (string.IsNullOrEmpty(newMetadata))
                return;

            ResultInfo result = SaveTridionObjectMetadata(client, sourceMetadataSchema, tridionObject.Title, newMetadata, tridionObject.LocationInfo.OrganizationalItem.IdRef, localize);
            results.Add(result);
        }

        public static void TransformTridionObjectMetadata(IAlchemyCoreServiceClient client, string sourceTridionObjectUri, string sourceContainerUri, string sourceMetadataSchemaUri, string targetFolderUri, string targetSchemaUri, string newTitle, bool localize, List<FieldMappingInfo> fieldMapping, List<FieldMappingInfo> metadataFieldMapping, TargetLinkInfo targetLink, List<ResultInfo> results)
        {
            if (string.IsNullOrEmpty(sourceTridionObjectUri))
                return;

            // Open up the source metadata schema 
            SchemaData sourceMetadataSchema = client.Read(sourceMetadataSchemaUri, null) as SchemaData;
            if (sourceMetadataSchema == null)
                return;

            List<ItemFieldDefinitionData> sourceMetadataFields = GetSchemaMetadataFields(client, sourceMetadataSchemaUri);

            // Open up the target component schema
            SchemaData targetSchema = client.Read(targetSchemaUri, null) as SchemaData;
            if (targetSchema == null)
                return;

            List<ItemFieldDefinitionData> targetComponentFields = GetSchemaFields(client, targetSchemaUri);
            List<ItemFieldDefinitionData> targetMetadataFields = GetSchemaMetadataFields(client, targetSchemaUri);

            // Change schema for component
            TransformTridionObjectMetadata(client, sourceTridionObjectUri, sourceContainerUri, sourceMetadataSchema, sourceMetadataFields, targetFolderUri, targetSchema, targetComponentFields, targetMetadataFields, newTitle, localize, fieldMapping, metadataFieldMapping, targetLink, results);
        }

        private static void TransformTridionObjectMetadata(IAlchemyCoreServiceClient client, string sourceTridionObjectUri, string sourceContainerUri, SchemaData sourceMetadataSchema, List<ItemFieldDefinitionData> sourceMetadataFields, string targetFolderUri, SchemaData targetSchema, List<ItemFieldDefinitionData> targetComponentFields, List<ItemFieldDefinitionData> targetMetadataFields, string newTitle, bool localize, List<FieldMappingInfo> fieldMapping, List<FieldMappingInfo> metadataFieldMapping, TargetLinkInfo targetLink, List<ResultInfo> results)
        {
            if (string.IsNullOrEmpty(sourceTridionObjectUri))
                return;

            if (results == null)
                results = new List<ResultInfo>();

            RepositoryLocalObjectData tridionObject = ReadItem(client, sourceTridionObjectUri) as RepositoryLocalObjectData;
            if (tridionObject == null || string.IsNullOrEmpty(tridionObject.Metadata))
                return;

            if (!tridionObject.MetadataSchema.IdRef.GetId().Equals(sourceMetadataSchema.Id.GetId()))
            {
                // If the tridion object is not of the metadata schema, do nothing...
                return;
            }

            // create folder chain
            if (!string.IsNullOrEmpty(sourceContainerUri))
            {
                FolderData sourceFolder = client.Read(sourceContainerUri, new ReadOptions()) as FolderData;
                if (sourceFolder != null)
                {
                    List<string> tridionObjectWebDavChain = tridionObject.GetWebDavChain();
                    List<string> folderChain = tridionObjectWebDavChain.Take(tridionObjectWebDavChain.Count - 1).ToList().Substract(sourceFolder.GetWebDavChain());
                    targetFolderUri = CreateFolderChain(client, folderChain, targetFolderUri);
                }
            }

            //get fixed xml
            string newContent = GetFixedContent(client, new List<string> { tridionObject.Metadata }, sourceMetadataSchema.NamespaceUri, tridionObject.ToItem(), newTitle, targetSchema, targetSchema.RootElementName, targetComponentFields, targetFolderUri, fieldMapping, results);

            //get fixed metadata
            string newMetadata = GetFixedContent(client, new List<string> { tridionObject.Metadata }, sourceMetadataSchema.NamespaceUri, tridionObject.ToItem(), newTitle, targetSchema, "Metadata", targetMetadataFields, targetFolderUri, metadataFieldMapping, results);

            if (string.IsNullOrEmpty(newContent) && string.IsNullOrEmpty(newMetadata))
                return;

            ResultInfo result = SaveComponent(client, null, null, null, targetSchema, newTitle, newContent, newMetadata, targetFolderUri, localize);
            results.Add(result);

            //save component link to target component to source metadata fields, if "target component link" selected

            if (result.Status == Status.Success && !string.IsNullOrEmpty(result.Item.TcmId) && targetLink != null && !string.IsNullOrEmpty(targetLink.Path))
            {
                var newMetadataFieldMapping = GetDefaultFieldMapping(client, sourceMetadataSchema, null, sourceMetadataFields);
                foreach (FieldMappingInfo newMapping in newMetadataFieldMapping)
                {
                    if (newMapping.TargetFieldPath == targetLink.Path)
                    {
                        newMapping.DefaultValue = result.Item.TcmId;
                        if(targetLink.Overwrite)
                            newMapping.SourceFieldPath = "create_new";
                    }
                }
                newMetadataFieldMapping = newMetadataFieldMapping.GetNestedFieldMapping("m.0", 0);

                FixTridionObjectMetadata(client, sourceTridionObjectUri, sourceMetadataSchema, sourceMetadataFields, targetFolderUri, localize, newMetadataFieldMapping, results);
            }
        }

        public static string GetTransformedName(IAlchemyCoreServiceClient client, string sourceUri, string formatString, List<ReplacementInfo> replacements)
        {
            string title = null;
            string content = null;
            string metadata = null;
            XNamespace ns = string.Empty;

            ItemType itemtype = GetItemType(sourceUri);
            if (itemtype == ItemType.Component)
            {
                ComponentData component = GetComponent(client, sourceUri);
                title = component.Title;
                content = component.Content;
                metadata = component.Metadata;

                string schemaUri = component.Schema.IdRef;
                SchemaData schema = ReadItem(client, schemaUri) as SchemaData;
                if (schema != null)
                    ns = schema.NamespaceUri;
            }
            else
            {
                RepositoryLocalObjectData item = ReadItem(client, sourceUri) as RepositoryLocalObjectData;
                if (item != null)
                {
                    title = item.Title;
                    metadata = item.Metadata;

                    string schemaUri = item.MetadataSchema.IdRef;
                    SchemaData schema = ReadItem(client, schemaUri) as SchemaData;
                    if (schema != null)
                        ns = schema.NamespaceUri;
                }
            }

            return GetTransformedName(title, sourceUri, content, metadata, ns, formatString, replacements);
        }

        public static string GetTransformedName(string title, string sourceUri, string content, string metadata, XNamespace ns, string formatString, List<ReplacementInfo> replacements)
        {
            if (replacements == null || replacements.Count == 0)
                return title;

            XDocument docContent = string.IsNullOrEmpty(content) ? null : XDocument.Parse(content);
            XElement contentXml = docContent == null || docContent.Root == null ? null : docContent.Root;

            XDocument docMetadata = string.IsNullOrEmpty(metadata) ? null : XDocument.Parse(metadata);
            XElement metadataXml = docMetadata == null || docMetadata.Root == null ? null : docMetadata.Root;

            List<object> replacementResults = new List<object>();
            foreach (ReplacementInfo replacement in replacements)
            {
                if (replacement.FieldPath == "[Title]")
                {
                    replacementResults.Add(string.IsNullOrEmpty(replacement.Regex) ? title : Regex.Match(title, replacement.Regex).Value);
                }
                else if (replacement.FieldPath == "[TcmId]")
                {
                    replacementResults.Add(string.IsNullOrEmpty(replacement.Regex) ? sourceUri : Regex.Match(sourceUri, replacement.Regex).Value);
                }
                else if (replacement.FieldPath == "[ID]")
                {
                    replacementResults.Add(string.IsNullOrEmpty(replacement.Regex) ? sourceUri.GetId() : Regex.Match(sourceUri.GetId(), replacement.Regex).Value);
                }
                else if (!string.IsNullOrEmpty(replacement.FieldPath) && contentXml != null)
                {
                    XElement field = contentXml.GetByXPath(replacement.FieldPath, ns);
                    if (field != null)
                    {
                        replacementResults.Add(string.IsNullOrEmpty(replacement.Regex) ? field.Value : Regex.Match(field.Value.ToString(), replacement.Regex).Value);
                    }
                }
                else if (!string.IsNullOrEmpty(replacement.FieldPath) && metadataXml != null)
                {
                    XElement field = metadataXml.GetByXPath(replacement.FieldPath, ns);
                    if (field != null)
                    {
                        replacementResults.Add(string.IsNullOrEmpty(replacement.Regex) ? field.Value : Regex.Match(field.Value.ToString(), replacement.Regex).Value);
                    }
                }
            }

            return string.Format(formatString, replacementResults.ToArray());
        }

        #endregion

        #region Tridion Blueprint

        public static string GetBluePrintLocalizedTcmId(RepositoryLocalObjectData dataItem)
        {
            string localizedPublication = dataItem.BluePrintInfo.OwningRepository.IdRef;
            return GetBluePrintItemTcmId(dataItem.Id, localizedPublication);
        }

        private static void Localize(IAlchemyCoreServiceClient client, ItemInfo item)
        {
            if (!item.IsLocalized)
                client.Localize(item.TcmId, new ReadOptions());
        }

        #endregion

        #region Collection helpers

        public static List<ItemInfo> ToList(this XElement xml, ItemType itemType)
        {
            List<ItemInfo> res = new List<ItemInfo>();
            if (xml != null && xml.HasElements)
            {
                foreach (XElement element in xml.Elements())
                {
                    ItemInfo item = new ItemInfo();
                    item.TcmId = element.Attribute("ID").Value;
                    item.ItemType = itemType;
                    item.Title = element.Attributes().Any(x => x.Name == "Title") ? element.Attribute("Title").Value : item.TcmId;
                    item.Icon = element.Attributes().Any(x => x.Name == "Icon") ? element.Attribute("Icon").Value : "T16L0P0";
                    item.Path = element.Attributes().Any(x => x.Name == "Path") ? element.Attribute("Path").Value : string.Empty;

                    if (item.ItemType == ItemType.Schema)
                    {
                        if (element.Attributes().Any(x => x.Name == "Icon"))
                        {
                            string icon = element.Attribute("Icon").Value;
                            if (icon.EndsWith("S7"))
                            {
                                item.SchemaType = SchemaType.Bundle;
                            }
                            else if (icon.EndsWith("S6"))
                            {
                                item.SchemaType = SchemaType.Parameters;
                            }
                            else if (icon.EndsWith("S3"))
                            {
                                item.SchemaType = SchemaType.Metadata;
                            }
                            else if (icon.EndsWith("S2"))
                            {
                                item.SchemaType = SchemaType.Embedded;
                            }
                            else if (icon.EndsWith("S1"))
                            {
                                item.SchemaType = SchemaType.Multimedia;
                            }
                            else if (icon.EndsWith("S0"))
                            {
                                item.SchemaType = SchemaType.Component;
                            }
                            else
                            {
                                item.SchemaType = SchemaType.None;
                            }
                        }
                        else
                        {
                            item.SchemaType = SchemaType.None;
                        }
                    }
                    else
                    {
                        item.SchemaType = SchemaType.None;
                    }

                    if (item.ItemType == ItemType.Component)
                    {
                        item.MimeType = element.Attributes().Any(x => x.Name == "MIMEType") ? element.Attribute("MIMEType").Value : null;
                    }
                    
                    item.FromPub = element.Attributes().Any(x => x.Name == "FromPub") ? element.Attribute("FromPub").Value : null;
                    item.IsPublished = element.Attributes().Any(x => x.Name == "Icon") && element.Attribute("Icon").Value.EndsWith("P1");
                    
                    res.Add(item);
                }
            }
            return res;
        }

        public static List<ItemInfo> ToList(this XElement xml)
        {
            List<ItemInfo> res = new List<ItemInfo>();
            if (xml != null && xml.HasElements)
            {
                foreach (XElement element in xml.Elements())
                {
                    ItemInfo item = new ItemInfo();
                    item.TcmId = element.Attribute("ID").Value;
                    item.ItemType = element.Attributes().Any(x => x.Name == "Type") ? (ItemType)int.Parse(element.Attribute("Type").Value) : GetItemType(item.TcmId);
                    item.Title = element.Attributes().Any(x => x.Name == "Title") ? element.Attribute("Title").Value : item.TcmId;
                    item.Icon = element.Attributes().Any(x => x.Name == "Icon") ? element.Attribute("Icon").Value : "T16L0P0";
                    item.Path = element.Attributes().Any(x => x.Name == "Path") ? element.Attribute("Path").Value : string.Empty;

                    if (item.ItemType == ItemType.Schema)
                    {
                        if (element.Attributes().Any(x => x.Name == "Icon"))
                        {
                            string icon = element.Attribute("Icon").Value;
                            if (icon.EndsWith("S7"))
                            {
                                item.SchemaType = SchemaType.Bundle;
                            }
                            else if (icon.EndsWith("S6"))
                            {
                                item.SchemaType = SchemaType.Parameters;
                            }
                            else if (icon.EndsWith("S3"))
                            {
                                item.SchemaType = SchemaType.Metadata;
                            }
                            else if (icon.EndsWith("S2"))
                            {
                                item.SchemaType = SchemaType.Embedded;
                            }
                            else if (icon.EndsWith("S1"))
                            {
                                item.SchemaType = SchemaType.Multimedia;
                            }
                            else if (icon.EndsWith("S0"))
                            {
                                item.SchemaType = SchemaType.Component;
                            }
                            else
                            {
                                item.SchemaType = SchemaType.None;
                            }
                        }
                        else
                        {
                            item.SchemaType = SchemaType.None;
                        }
                    }
                    else
                    {
                        item.SchemaType = SchemaType.None;
                    }

                    if (item.ItemType == ItemType.Component)
                    {
                        item.MimeType = element.Attributes().Any(x => x.Name == "MIMEType") ? element.Attribute("MIMEType").Value : null;    
                    }

                    item.FromPub = element.Attributes().Any(x => x.Name == "FromPub") ? element.Attribute("FromPub").Value : null;
                    item.IsPublished = element.Attributes().Any(x => x.Name == "Icon") && element.Attribute("Icon").Value.EndsWith("P1");
                    
                    res.Add(item);
                }
            }
            return res;
        }

        public static ItemInfo ToItem(this RepositoryLocalObjectData dataItem, string topTcmId = null)
        {
            ItemInfo item = new ItemInfo();
            item.TcmId = dataItem.Id;
            item.ItemType = GetItemType(dataItem.Id);
            item.Title = dataItem.Title;

            string webDav = dataItem.GetWebDav();
            item.Path = string.IsNullOrEmpty(webDav) ? string.Empty : Path.GetDirectoryName(webDav.Replace('/', '\\'));

            if (item.ItemType == ItemType.Schema)
            {
                SchemaData schemaDataItem = (SchemaData)dataItem;
                if (schemaDataItem.Purpose == SchemaPurpose.Bundle)
                {
                    item.SchemaType = SchemaType.Bundle;
                }
                else if (schemaDataItem.Purpose == SchemaPurpose.TemplateParameters)
                {
                    item.SchemaType = SchemaType.Parameters;
                }
                else if (schemaDataItem.Purpose == SchemaPurpose.Metadata)
                {
                    item.SchemaType = SchemaType.Metadata;
                }
                else if (schemaDataItem.Purpose == SchemaPurpose.Embedded)
                {
                    item.SchemaType = SchemaType.Embedded;
                }
                else if (schemaDataItem.Purpose == SchemaPurpose.Multimedia)
                {
                    item.SchemaType = SchemaType.Multimedia;
                }
                else if (schemaDataItem.Purpose == SchemaPurpose.Component)
                {
                    item.SchemaType = SchemaType.Component;
                }
                else
                {
                    item.SchemaType = SchemaType.None;
                }
            }

            if (GetPublicationTcmId(dataItem.Id) == dataItem.BluePrintInfo.OwningRepository.IdRef && dataItem.Id == topTcmId)
                item.FromPub = string.Empty;
            else if (GetPublicationTcmId(dataItem.Id) == dataItem.BluePrintInfo.OwningRepository.IdRef)
                item.FromPub = "(Local copy)";
            else
                item.FromPub = dataItem.BluePrintInfo.OwningRepository.Title;

            if (dataItem.IsPublishedInContext != null)
                item.IsPublished = dataItem.IsPublishedInContext.Value;

            item.Icon = "T" + (int)item.ItemType + "L0P" + (item.IsPublished ? "1" : "0");

            if (item.ItemType == ItemType.Component)
            {
                ComponentData componentDataItem = (ComponentData)dataItem;
                if (componentDataItem.ComponentType == ComponentType.Multimedia)
                {
                    item.MimeType = componentDataItem.BinaryContent.MimeType;

                    if (componentDataItem.BinaryContent.Filename != null)
                    {
                        string ext = Path.GetExtension(componentDataItem.BinaryContent.Filename);
                        item.Icon += "M" + ext.Trim('.');
                    }
                }
            }

            return item;
        }

        public static ItemInfo ToItem(this PublicationData publicationData)
        {
            ItemInfo item = new ItemInfo();
            item.TcmId = publicationData.Id;
            item.ItemType = ItemType.Publication;
            item.Title = publicationData.Title;
            item.Icon = "T1L0P0S0";

            return item;
        }

        public static ItemType GetItemType(string tcmItem)
        {
            if (string.IsNullOrEmpty(tcmItem))
                return ItemType.None;

            string[] arr = tcmItem.Split('-');
            if (arr.Length == 2 || arr.Length == 3 && arr[2].StartsWith("v")) return ItemType.Component;

            return (ItemType)int.Parse(arr[2]);
        }

        public static FieldType GetFieldType(this ItemFieldDefinitionData field)
        {
            if (field == null)
            {
                return FieldType.None;
            }
            if (field is SingleLineTextFieldDefinitionData)
            {
                return FieldType.SingleLineText;
            }
            if (field is MultiLineTextFieldDefinitionData)
            {
                return FieldType.MultiLineText;
            }
            if (field is XhtmlFieldDefinitionData)
            {
                return FieldType.Xhtml;
            }
            if (field is DateFieldDefinitionData)
            {
                return FieldType.Date;
            }
            if (field is NumberFieldDefinitionData)
            {
                return FieldType.Number;
            }
            if (field is KeywordFieldDefinitionData)
            {
                return FieldType.Keyword;
            }
            if (field is MultimediaLinkFieldDefinitionData)
            {
                return FieldType.Multimedia;
            }
            if (field is ExternalLinkFieldDefinitionData)
            {
                return FieldType.ExternalLink;
            }
            if (field is ComponentLinkFieldDefinitionData)
            {
                return FieldType.ComponentLink;
            }
            if (field is EmbeddedSchemaFieldDefinitionData)
            {
                return FieldType.EmbeddedSchema;
            }
            
            return FieldType.None;
        }

        public static string GetFieldTypeName(this ItemFieldDefinitionData field)
        {
            return field.GetFieldType() == FieldType.None ? string.Empty : field.GetFieldType().ToString();
        }

        public static bool IsText(this ItemFieldDefinitionData field)
        {
            return field is SingleLineTextFieldDefinitionData && !field.IsTextSelect() || field is MultiLineTextFieldDefinitionData;
        }

        public static bool IsRichText(this ItemFieldDefinitionData field)
        {
            return field is XhtmlFieldDefinitionData;
        }

        public static bool IsDate(this ItemFieldDefinitionData field)
        {
            return field is DateFieldDefinitionData;
        }

        public static bool IsNumber(this ItemFieldDefinitionData field)
        {
            return field is NumberFieldDefinitionData;
        }

        public static bool IsKeyword(this ItemFieldDefinitionData field)
        {
            return field is KeywordFieldDefinitionData;
        }

        public static bool IsMultimedia(this ItemFieldDefinitionData field)
        {
            return field is MultimediaLinkFieldDefinitionData;
        }

        public static bool IsTextSelect(this ItemFieldDefinitionData field)
        {
            if (field is SingleLineTextFieldDefinitionData)
            {
                SingleLineTextFieldDefinitionData textField = (SingleLineTextFieldDefinitionData)field;
                return textField.List != null && textField.List.Entries != null && textField.List.Entries.Length > 0;
            }
            return false;
        }

        public static bool IsEmbedded(this ItemFieldDefinitionData field)
        {
            return field is EmbeddedSchemaFieldDefinitionData;
        }

        public static bool IsComponentLink(this ItemFieldDefinitionData field)
        {
            return field is ComponentLinkFieldDefinitionData;
        }

        public static bool IsMultimediaComponentLink(this ItemFieldDefinitionData field)
        {
            ComponentLinkFieldDefinitionData clField = field as ComponentLinkFieldDefinitionData;
            if (clField == null)
                return false;
            return clField.AllowMultimediaLinks;
        }

        public static bool IsMultiValue(this ItemFieldDefinitionData field)
        {
            return field.MaxOccurs == -1 || field.MaxOccurs > 1;
        }
        
        public static bool IsMandatory(this ItemFieldDefinitionData field)
        {
            return field.MinOccurs == 1;
        }

        public static bool IsPrimitive(this ItemFieldDefinitionData field)
        {
            FieldType fieldType = field.GetFieldType();

            return fieldType == FieldType.SingleLineText ||
                fieldType == FieldType.MultiLineText ||
                fieldType == FieldType.Xhtml || 
                fieldType == FieldType.Date ||
                fieldType == FieldType.Number ||
                fieldType == FieldType.Keyword ||
                fieldType == FieldType.ExternalLink;
        }

        public static bool IsLink(this ItemFieldDefinitionData field)
        {
            FieldType fieldType = field.GetFieldType();

            return fieldType == FieldType.Multimedia ||
                fieldType == FieldType.ComponentLink;
        }

        #endregion

        #region Text helpers

        public static string GetId(this string tcmId)
        {
            if (string.IsNullOrEmpty(tcmId) || !tcmId.StartsWith("tcm:") || !tcmId.Contains("-"))
                return string.Empty;

            return tcmId.Split('-')[1];
        }

        public static string GetPublicationTcmId(string id)
        {
            ItemType itemType = GetItemType(id);
            if (itemType == ItemType.Publication)
                return id;
            
            return "tcm:0-" + id.Replace("tcm:", string.Empty).Split('-')[0] + "-1";
        }

        public static string GetBluePrintItemTcmId(string id, string publicationId)
        {
            if (string.IsNullOrEmpty(id) || !id.StartsWith("tcm:") || !id.Contains("-") || string.IsNullOrEmpty(publicationId) || !publicationId.StartsWith("tcm:") || !publicationId.Contains("-"))
                return string.Empty;

            return "tcm:" + publicationId.GetId() + "-" + id.GetId() + (id.Split('-').Length > 2 ? "-" + id.Split('-')[2] : string.Empty);
        }

        public static string CutPath(this string path, string separator, int maxLength)
        {
            if (path == null || path.Length <= maxLength)
                return path;

            var list = path.Split(new[] { separator[0] });
            int itemMaxLength = maxLength / list.Length;

            return String.Join(separator, list.Select(item => item.Cut(itemMaxLength)).ToList());
        }

        public static string CutPath(this string path, string separator, int maxLength, bool fullLastItem)
        {
            if (path == null || path.Length <= maxLength)
                return path;

            if (!fullLastItem)
                return path.CutPath(separator, maxLength);

            string lastItem = path.Substring(path.LastIndexOf(separator, StringComparison.Ordinal));

            if (lastItem.Length > maxLength)
                return path.CutPath(separator, maxLength);

            return path.Substring(0, path.LastIndexOf(separator, StringComparison.Ordinal)).CutPath(separator, maxLength - lastItem.Length) + lastItem;
        }

        public static string Cut(this string str, int maxLength)
        {
            if (maxLength < 5)
                maxLength = 5;

            if (str.Length > maxLength)
            {
                return str.Substring(0, maxLength - 2) + "..";

            }
            return str;
        }

        public static string PrettyXml(this string xml)
        {
            try
            {
                if (string.IsNullOrEmpty(xml))
                    return xml;

                return XElement.Parse(xml).ToString().Replace(" xmlns=\"\"", "");
            }
            catch (Exception)
            {
                return xml;
            }
        }

        public static string PlainXml(this string xml)
        {
            try
            {
                if (string.IsNullOrEmpty(xml))
                    return xml;

                return Regex.Replace(xml, "\\s+", " ").Replace("> <", "><");
            }
            catch (Exception)
            {
                return xml;
            }
        }

        public static string GetInnerXml(this XElement node)
        {
            var reader = node.CreateReader();
            reader.MoveToContent();
            return reader.ReadInnerXml();
        }

        private static string GetLocalizedXml(this string content, string anyLocalTcm)
        {
            string res = content;

            string targetPrefix = anyLocalTcm.Split('-')[0] + "-";
            List<string> prefixes = Regex.Matches(res, "tcm:(\\d)+-").Cast<Match>().Select(x => x.Value).ToList();
            foreach (string prefix in prefixes)
            {
                res = res.Replace(prefix, targetPrefix);
            }

            return res;
        }

        public static string GetFieldName(this ItemFieldDefinitionData field)
        {
            if (field.IsEmbedded())
                return string.Format("{0} | {1}", field.Name, ((EmbeddedSchemaFieldDefinitionData)field).EmbeddedSchema.Title);

            if (field.IsComponentLink())
            {
                ComponentLinkFieldDefinitionData componentLinkField = (ComponentLinkFieldDefinitionData)field;
                if (componentLinkField.AllowedTargetSchemas.Any())
                    return string.Format("{0} | {1}", componentLinkField.Name, componentLinkField.AllowedTargetSchemas[0].Title);
            }

            return string.Format("{0} | {1}", field.Name, field.GetFieldTypeName());
        }

        public static string GetDomainName(this string url)
        {
            if (!url.Contains(Uri.SchemeDelimiter))
            {
                url = string.Concat(Uri.UriSchemeHttp, Uri.SchemeDelimiter, url);
            }
            Uri uri = new Uri(url);
            return uri.Host;
        }

        public static bool IsHtml(this string html)
        {
            Regex tagRegex = new Regex(@"<[^>]+>");
            return tagRegex.IsMatch(html);
        }

        #endregion

    }
}