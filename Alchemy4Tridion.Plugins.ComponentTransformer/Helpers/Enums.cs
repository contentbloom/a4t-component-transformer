﻿namespace Alchemy4Tridion.Plugins.ComponentTransformer.Helpers
{
    public enum Status
    {
        Info = 1,
        Success = 2,
        Warning = 4,
        Error = 8,
        None = 0
    }

    public enum FieldType
    {
        SingleLineText,
        MultiLineText,
        Xhtml,
        Date,
        Number,
        Keyword,
        Multimedia,
        ExternalLink,
        ComponentLink,
        EmbeddedSchema,
        None
    }

    public enum SchemaType
    {
        Any = 4,

        Component = 0,
        Multimedia = 1,
        Embedded = 2,
        Metadata = 3,
        Parameters = 6,
        Bundle = 7,

        None = 5
    }

    public enum ObjectType
    {
        Any,
        Component,
        Folder,
        ComponentOrFolder,
        Page,
        StructureGroup,
        PageOrStructureGroup
    }

    public enum LinkStatus
    {
        Found,
        NotFound,
        Mandatory,
        Error
    }
}