﻿<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Save Target Link Back to Source Field</title>
        <link rel='shortcut icon' type='image/x-icon' href='${ImgUrl}favicon.png' />
    </head>
    <body>

        <div class="tab-body active">

            <div class="block-field settings">
                <label for="targetLink">
                    <span class="asterisk">*</span>
                    Source Field:
                </label>
                <div>
                    <div class="itemselect">
                        <select id="targetLink"></select>
                    </div>
                    <div class="field-description">
                        Select component link field from source component/metadata
                    </div>
                </div>
            </div>

            <div class="settings" style="white-space: nowrap;">
                <input type="checkbox" id="targetLinkOverwrite" />
                <label for="targetLinkOverwrite">
                    Overwrite existing component link
                </label>
            </div>

        </div>
        
        <div class="controls">
            <div class="button disabled" id="ok"><span class="text">Ok</span></div>
            <div class="button enabled" id="close_window"><span class="text">Close</span></div>
        </div>
        
    </body>
</html>