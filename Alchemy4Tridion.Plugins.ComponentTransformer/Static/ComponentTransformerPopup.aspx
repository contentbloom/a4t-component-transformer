﻿<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Component Transformer</title>
        <link rel='shortcut icon' type='image/x-icon' href='${ImgUrl}favicon.png' />
    </head>
    <body style="height: 96% !important;">
        
        <div class="tab-body active">
            
            <progress id="progBar"></progress>
            
            <input type="hidden" id="sourceId" />
            <div class="block-field settings">
                <label>
                    <span class="asterisk">*</span>
                    Source:
                </label>
                <div>
                    <div class="itemselect">
                        <div style="margin-right: 34px; background-image: none;">
                            <span id="sourceWebdav" style="padding-left: 18px; background-repeat: no-repeat;"></span>
                            <div class="buttons">
                                <div id="openSource" class="tridion button imageButton browse button2013 gray imageButton noTextButton" title="Browse">
                                    <div class="image"></div>
                                    <span class="text">&nbsp;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field-description">
                        Select source folder/component/structure group/page/category/keyword
                    </div>
                </div>
            </div>

            <div class="block-field settings" id="sourceItemsCountContainer">
                <div>
                    <div class="field-description-bold">
                        <span id="sourceItemsCount"></span>
                    </div>
                </div>
            </div>

            <div class="block-field" id="progressContainer">
                <div>
                    <div class="field-description">
                        <span id="progress"></span>
                    </div>
                </div>
            </div>
            
            <input type="hidden" id="targetId" />
            <div class="block-field settings">
                <label>
                    <span class="asterisk">*</span>
                    Target:
                </label>
                <div>
                    <div class="itemselect">
                        <div style="margin-right: 34px; background-image: none;">
                            <span id="targetWebdav" style="padding-left: 18px; background-repeat: no-repeat;"></span>
                            <div class="buttons">
                                <div id="openTarget" class="tridion button imageButton browse button2013 gray imageButton noTextButton" title="Browse">
                                    <div class="image"></div>
                                    <span class="text">&nbsp;</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field-description">
                        Select target folder
                    </div>
                </div>
            </div>
            
            <div class="block-field settings">
                <label for="sourceSchema">
                    <span class="asterisk">*</span>
                    Source Schema:
                </label>
                <div>
                    <div class="itemselect">
                        <select id="sourceSchema"></select>
                    </div>
                    <div class="field-description">
                        Select source schema
                    </div>
                </div>
            </div>

            <div class="block-field settings">
                <label for="targetSchema">
                    <span class="asterisk">*</span>
                    Target Schema:
                </label>
                <div>
                    <div class="itemselect">
                        <select id="targetSchema"></select>
                    </div>
                    <div class="field-description">
                        Select target schema
                    </div>
                </div>
            </div>

            <br />
            
            <div class="button enabled settings" id="fieldMapping"><span class="text">Field Mapping</span></div>

            <div class="button enabled settings" id="metadataMapping"><span class="text">Metadata Field Mapping</span></div>

            <div class="button enabled settings" id="nameTransform"><span class="text">Name Transform</span></div>

            <div class="button enabled settings" id="targetLink"><span class="text">Target Link</span></div>

            <%--<div class="button enabled settings" id="customTransform"><span class="text">Custom Transform</span></div>--%>

            <br />
            <br />
            <br />
            <div class="settings" style="white-space: nowrap;">
                <input type="checkbox" id="localizeTarget" />
                <label for="localizeTarget">
                    Localize target if exists
                </label>
            </div>
            
        </div>

        <div class="controls">
            <div class="button disabled" id="transform"><span class="text">Start</span></div>
            <div class="button enabled" id="close_window"><span class="text">Close</span></div>
        </div>
        
    </body>
</html>