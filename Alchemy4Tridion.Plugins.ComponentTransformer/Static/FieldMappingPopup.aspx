﻿<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Field Mapping</title>
        <link rel='shortcut icon' type='image/x-icon' href='${ImgUrl}favicon.png' />
    </head>
    <body>

        <div class="tab-body active">
            <table class="usingItems results" id="fieldMappingForm">
            
            </table>
        </div>
        
        <div class="controls">
            <div class="button disabled" id="ok"><span class="text">Ok</span></div>
            <div class="button enabled" id="close_window"><span class="text">Close</span></div>
        </div>
        
    </body>
</html>