﻿<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Name Transform</title>
        <link rel='shortcut icon' type='image/x-icon' href='${ImgUrl}favicon.png' />
    </head>
    <body>

        <div class="tab-body active">
            <table class="usingItems results" id="fieldMappingForm">

                <tr>
                    <td class="name" style="text-align: right;"><label>Format String:</label></td>
                    <td class="path" colspan="2"><input id="formatString" type="text" style="width: 100%;" /></td>
                </tr>

                <tr class="replacement" id="replacement0" data-id="0">
                    <td class="name" style="text-align: right;"><label>{0}:</label></td>
                    <td class="path"><select /></td>
                    <td class="operation"><input type="text" /></td>
                </tr>

                <tr class="replacement" id="replacement1" data-id="1">
                    <td class="name" style="text-align: right;"><label>{1}:</label></td>
                    <td class="path"><select /></td>
                    <td class="operation"><input type="text" /></td>
                </tr>

                <tr class="replacement" id="replacement2" data-id="2">
                    <td class="name" style="text-align: right;"><label>{2}:</label></td>
                    <td class="path"><select /></td>
                    <td class="operation"><input type="text" /></td>
                </tr>
            
            </table>
        </div>
        
        <div class="controls">
            <div class="button disabled" id="ok"><span class="text">Ok</span></div>
            <div class="button enabled" id="close_window"><span class="text">Close</span></div>
        </div>
        
    </body>
</html>