﻿Type.registerNamespace("Alchemy4Tridion.Plugins.ComponentTransformer");

!(function () {

    $j = Alchemy.library("jQuery");

    console.log("Intit settings", "Intit settings");

    var item = $display.getItem();
    var itemId = item.properties.id;
    var tcm = itemId.replace("tcm:", "");

    loadSettings(tcm);

    function loadSettings(sourceId) {

        if (!sourceId)
            sourceId = $j('#sourceId').val();

        var targetId = $j('#targetId').val();
        if (!targetId)
            targetId = getCookie(sourceId + '_targetId');
        if (!targetId)
            targetId = sourceId;

        //init global mapping
        window.globalSettings = null;
        window.globalMapping = null;
        window.globalMetadataMapping = null;
        window.globalNameTransform = null;
        window.globalTargetLinkPath = null;
        window.globalTargetLinkOverwrite = false;

        //disable buttons untill schemas loaded

        $j('#sourceSchema').unbind();

        $j('#targetSchema').unbind();

        checkFieldMappingAvailability(null, "");

        $j("#nameTransform").removeClass("enabled");
        $j("#nameTransform").addClass("disabled");
        $j("#nameTransform").unbind();

        //disable main buttons
        refreshButtons();

        $j('#sourceItemsCountContainer').hide();
        $j('#progressContainer').hide();

        //enable progress bar
        $j("#progBar").show();

        // This is the call to my controller
        Alchemy.Plugins["${PluginName}"].Api.ComponentTransformerService.getSettings(sourceId, targetId).success(function (settings) {

            //save settings to global variable
            window.globalSettings = settings;

            //disable progress bar
            $j("#progBar").hide();

            //load settings

            sourceId = settings.sourceId.replace("tcm:", "");
            targetId = settings.targetId.replace("tcm:", "");

            $j('#sourceId').val(sourceId);
            $j('#sourceWebdav').text(settings.sourceWebdav);
            $j('#sourceWebdav').prop('title', settings.sourceWebdav);
            $j('#sourceWebdav').css('background-image', 'url(/WebUI/Editors/CME/Themes/Carbon2/icon_v7.1.0.66.627_.png?name=' + settings.sourceIcon + '&size=16)')
            
            $j('#targetId').val(targetId);
            $j('#targetWebdav').text(settings.targetWebdav);
            $j('#targetWebdav').prop('title', settings.targetWebdav);
            $j('#targetWebdav').css('background-image', 'url(/WebUI/Editors/CME/Themes/Carbon2/icon_v7.1.0.66.627_.png?name=' + settings.targetIcon + '&size=16)')

            $j('#sourceSchema').empty();
            $j.each(settings.sourceSchemas, function (i, item) {
                $j('#sourceSchema').append($j('<option>', {
                    value: item.tcmId,
                    text: item.title
                }));
            });

            var sourceSchema = getCookie(sourceId + '_sourceSchema');
            if (!sourceSchema)
                sourceSchema = settings.sourceSchemaUri;
            $j('#sourceSchema').val(sourceSchema);

            //disable source schema combo if single item is selected
            var srcArr = sourceId.split('-');
            $j('#sourceSchema').prop("disabled", srcArr.length == 2 || srcArr.length == 3 && srcArr[2] != '2' && srcArr[2] != '4' && srcArr[2] != '512');

            $j('#targetSchema').empty();
            $j.each(settings.targetSchemas, function (i, item) {
                $j('#targetSchema').append($j('<option>', {
                    value: item.tcmId,
                    text: item.title
                }));
            });

            var targetSchema = getCookie(sourceId + '_targetSchema');
            if (!targetSchema)
                targetSchema = settings.targetSchemaUri;
            $j('#targetSchema').val(targetSchema);

            //call service and check if metadata is available
            checkFieldMappingAvailability(settings, targetSchema);

            //button handler - name transform dialog
            $j("#nameTransform").removeClass("disabled");
            $j("#nameTransform").addClass("enabled");
            $j("#nameTransform").click(function () {
                openNameTransformDialog();
            });

            //button handler - transform button + buttons coloring
            refreshButtons();

            //handlers

            $j('#sourceSchema').change(function () {

                setCookie(sourceId + '_sourceSchema', $j(this).val(), 7);

                //clear all popup settings
                globalMapping = null;
                globalMetadataMapping = null;
                globalNameTransform = null;
                globalTargetLinkPath = null;
                globalTargetLinkOverwrite = false;

                refreshButtons();
            });

            $j('#targetSchema').change(function () {

                setCookie(sourceId + '_targetSchema', $j(this).val(), 7);

                //clear all popup settings
                globalMapping = null;
                globalMetadataMapping = null;
                globalNameTransform = null;
                globalTargetLinkPath = null;
                globalTargetLinkOverwrite = false;

                //call service and check if metadata is available
                checkFieldMappingAvailability(settings, $j(this).val());

                refreshButtons();
            });

        })
        .error(function (error) {
            console.log("There was an error", error);
            $messages.registerError("An error has occurred", error && error.exceptionMessage ? error.exceptionMessage : null, null, true, false);
        })
        .complete(function () {
            
            //close window
            $j("#close_window").unbind();
            $j("#close_window").click(function () {
                closeWindow();
            });

            //open source dialog
            $j("#openSource").unbind();
            $j("#openSource").click(function () {
                openItemDialog();
            });

            //open target dialog
            $j("#openTarget").unbind();
            $j("#openTarget").click(function () {
                openFolderDialog();
            });

        });
    }

    function startTransform() {

        //enable progress bar
        $j("#progBar").show();

        //hide form
        $j(".settings").hide();

        $j("#transform").removeClass("enabled");
        $j("#transform").addClass("disabled");
        $j("#transform").unbind();

        //go to target location
        var folderUri = "tcm:" + globalSettings.targetId;
        refreshDashboard(folderUri);

        //start 1st item
        transformItem(0, globalSettings.sourceItemsCount);
    }

    function transformItem(index, count) {

        var item = globalSettings.sourceItems[index];
        var sourceFolderUri = globalSettings.sourceFolderUri;
        var targetFolderUri = "tcm:" + globalSettings.targetId;
        var sourceSchemaUri = $j('#sourceSchema').val();
        var targetSchemaUri = $j('#targetSchema').val();

        //show progress message
        $j('#progressContainer').show();
        $j('#progress').html(item.title + " ( " + (index + 1) + " of " + count + " )");

        var msg = $messages.registerProgress(item.title + " ( " + (index + 1) + " of " + count + " )", "Processing ...");
        //msg.setOnSuccessMessage(item.title + " ( " + (index + 1) + " of " + count + " )", "Processed");

        var formatString = globalNameTransform == null ? '' : globalNameTransform.formatString;
        var replacements = globalNameTransform == null ? null : globalNameTransform.replacements;
        var componentFieldMapping = globalMapping == null ? null : globalMapping.fieldMapping;
        var metadataFieldMapping = globalMetadataMapping == null ? null : globalMetadataMapping.fieldMapping;

        //call transform method
        Alchemy.Plugins["${PluginName}"].Api.ComponentTransformerService.transform({ SourceUri: item.tcmId, SourceFolderUri: sourceFolderUri, TargetFolderUri: targetFolderUri, SourceSchemaUri: sourceSchemaUri, TargetSchemaUri: targetSchemaUri, FormatString: formatString, Replacements: replacements, ComponentFieldMapping: componentFieldMapping, MetadataFieldMapping: metadataFieldMapping, TargetLink: { Path: globalTargetLinkPath, Overwrite: globalTargetLinkOverwrite }, Localize: $j('#localizeTarget').is(":checked") }).success(function (results) {

            //refresh dashboard
            refreshDashboard();

            var handled = false;

            //transform item message
            for (var j = 0; j < results.length; j++) {

                //error
                if (results[j].status == 8) {
                    $messages.registerError(item.title + " ( " + (index + 1) + " of " + count + " )", results[j].message, results[j].errorMessage, true, false);
                    handled = true;
                }
                    //info
                else if (results[j].status == 1) {
                    $messages.registerNotification(item.title + " ( " + (index + 1) + " of " + count + " )", results[j].message);
                    handled = true;
                }
                    //success
                else {
                    $messages.registerGoal(item.title + " ( " + (index + 1) + " of " + count + " )", results[j].message);
                    handled = true;
                }
            }

            if (!handled) {
                $messages.registerNotification(item.title + " ( " + (index + 1) + " of " + count + " )", "Item was skipped");
            }
        })
        .error(function (error) {
            console.log("There was an error", error);
            $messages.registerError(item.title + " ( " + (index + 1) + " of " + count + " )", error && error.exceptionMessage ? error.exceptionMessage : "An error has occurred", null, true, false);
        })
        .complete(function () {
            // this is called regardless of success or failure.
            msg.finish();

            if (index + 1 < count) {

                //transform next item
                transformItem(index + 1, count);
            }
            else {

                //disable progress bar
                $j("#progBar").hide();

                //hide progress message
                $j('#progressContainer').hide();

                //show form again
                $j(".settings").show();

                refreshButtons();
             }
        });
    }

    function closeWindow() {
        window.close();
    }

    function openItemDialog() {

        var sourceId = $j('#sourceId').val();
        var url = "/WebUI/Editors/CME/Views/Popups/ItemSelect/ItemSelectDialog.aspx#root=tcm:0&id=tcm:" + sourceId + "&locationId=" + globalSettings.sourceFolderUri;

        var popup = $popupManager.createExternalContentPopup(
            url,
            $cme.Popups.ITEM_SELECT.FEATURES,
            {
                filter: new Tridion.ContentManager.ListFilter({ conditions: { ItemTypes: [$const.ItemType.FOLDER, $const.ItemType.COMPONENT, $const.ItemType.STRUCTURE_GROUP, $const.ItemType.PAGE, $const.ItemType.CATEGORY, $const.ItemType.KEYWORD] } })
            });

        popup.open();

        $evt.addEventHandler(popup, "insert",
            function (event) {
                
                var items = event.data ? event.data.items : [];
                var sourceId = "";

                for (var i = 0, j = items.length; i < j; i++) {
                    sourceId = items[i];
                }

                sourceId = sourceId.substring(4);

                $j('#sourceId').val(sourceId);

                loadSettings();

            });
    }

    function openFolderDialog() {

        var sourceId = $j('#sourceId').val();
        var targetId = $j('#targetId').val();
        var url = "/WebUI/Editors/CME/Views/Popups/ItemSelect/ItemSelectDialog.aspx#root=tcm:0&id=tcm:" + targetId + "&locationId=" + globalSettings.targetFolderUri;

        var popup = $popupManager.createExternalContentPopup(
            url,
            $cme.Popups.ITEM_SELECT.FEATURES,
            {
                filter: new Tridion.ContentManager.ListFilter({ conditions: { ItemTypes: [$const.ItemType.FOLDER] } })
            });

        popup.open();

        $evt.addEventHandler(popup, "insert",
            function (event) {
                 
                var items = event.data ? event.data.items : [];
                var targetId = "";

                for (var i = 0, j = items.length; i < j; i++) {
                    targetId = items[i];
                }

                if (targetId) {
                    targetId = targetId.substring(4);
                    $j('#targetId').val(targetId);
                    setCookie(sourceId + '_targetId', targetId, 7);
                    loadSettings();
                }
            });
    }

    function openMappingDialog(metadata) {

        var sourceSchemaId = $j('#sourceSchema').val().replace("tcm:", "");
        var targetSchemaId = $j('#targetSchema').val().replace("tcm:", "");

        var url = "${ViewsUrl}FieldMappingPopup.aspx#sourceId=" + sourceSchemaId + "&targetId=" + targetSchemaId + '&metadata=' + (metadata ? 'true' : 'false');
        var popup = $popupManager.createExternalContentPopup(url, "width=900px,height=450px,resizable=0");

        $evt.addEventHandler(popup, "unload", function (e) {
            var popup = e.source;
            if (popup) {

                refreshButtons();

                popup.dispose();
                popup = null;
            }
        });

        popup.open();
    }

    function openNameTransformDialog() {

        var sourceSchemaId = $j('#sourceSchema').val().replace("tcm:", "");

        var url = "${ViewsUrl}NameTransformPopup.aspx#sourceId=" + sourceSchemaId;
        var popup = $popupManager.createExternalContentPopup(url, "width=700px,height=450px,resizable=0");

        $evt.addEventHandler(popup, "unload", function (e) {
            var popup = e.source;
            if (popup) {

                refreshButtons();

                popup.dispose();
                popup = null;
            }
        });

        popup.open();
    }

    function openTargetLinkDialog() {

        var sourceSchemaId = $j('#sourceSchema').val().replace("tcm:", "");

        var url = "${ViewsUrl}TargetLinkPopup.aspx#sourceId=" + sourceSchemaId;
        var popup = $popupManager.createExternalContentPopup(url, "width=700px,height=450px,resizable=0");

        $evt.addEventHandler(popup, "unload", function (e) {
            var popup = e.source;
            if (popup) {

                refreshButtons();

                popup.dispose();
                popup = null;
            }
        });

        popup.open();
    }

    function checkFieldMappingAvailability(settings, targetSchemaId) {

        //field mapping is disabled
        $j("#fieldMapping").removeClass("enabled");
        $j("#fieldMapping").addClass("disabled");
        $j("#fieldMapping").unbind();

        //metadata mapping is disabled
        $j("#metadataMapping").removeClass("enabled");
        $j("#metadataMapping").addClass("disabled");
        $j("#metadataMapping").unbind();

        if (!settings)
            return;

        var schema = settings.targetSchemas.filter(function (item) { return item.tcmId == targetSchemaId; })[0];

        //field mapping available only for target component schema
        if (schema && schema.schemaType == 0) {

            $j("#fieldMapping").removeClass("disabled");
            $j("#fieldMapping").addClass("enabled");
            $j("#fieldMapping").click(function () {
                openMappingDialog();
            });

            //check metadata mapping for component schema
            Alchemy.Plugins["${PluginName}"].Api.ComponentTransformerService.getMetadataFieldsCount(targetSchemaId.replace("tcm:", "")).success(function (count) {

                if (count > 0) {

                    //metadata mapping is enabled
                    $j("#metadataMapping").removeClass("disabled");
                    $j("#metadataMapping").addClass("enabled");
                    $j("#metadataMapping").click(function () {
                        openMappingDialog(true);
                    });
                }

            })
            .error(function (error) {
                console.log("There was an error", error);
                $messages.registerError("An error has occurred", error && error.exceptionMessage ? error.exceptionMessage : null, null, true, false);
            })
            .complete(function () {
                // this is called regardless of success or failure.
            });

        }
        else
        {
            //metadata mapping is enabled
            $j("#metadataMapping").removeClass("disabled");
            $j("#metadataMapping").addClass("enabled");
            $j("#metadataMapping").click(function () {
                openMappingDialog(true);
            });
        }
    }

    function refreshButtons() {

        $j("#fieldMapping").removeClass("green");
        $j("#metadataMapping").removeClass("green");
        $j("#nameTransform").removeClass("green");
        $j("#targetLink").removeClass("green");

        var sourceSchemaUri = $j('#sourceSchema').val();
        var targetSchemaUri = $j('#targetSchema').val();

        if (!sourceSchemaUri || !targetSchemaUri) {

            $j("#transform").removeClass("enabled");
            $j("#transform").addClass("disabled");
            $j("#transform").unbind();

            $j("#targetLink").removeClass("enabled");
            $j("#targetLink").addClass("disabled");
            $j("#targetLink").unbind();

            return;
        }

        //show info about operation

        if (globalSettings && globalSettings.sourceItemsCount) {

            $j('#sourceItemsCountContainer').show();

            if (sourceSchemaUri == targetSchemaUri) {
                if (globalNameTransform && globalNameTransform.formatString != "{0}") {
                    $j('#sourceItemsCount').html(globalSettings.sourceItemsCount + " items to be fixed and saved to another component");
                }
                else if (globalSettings && globalSettings.sourceFolderUri != "tcm:" + globalSettings.targetId) {
                    $j('#sourceItemsCount').html(globalSettings.sourceItemsCount + " items to be fixed and saved to another folder");
                }
                else {
                    $j('#sourceItemsCount').html(globalSettings.sourceItemsCount + " items to be fixed and saved back");
                }
            }
            else if (globalMapping || globalMetadataMapping) {
                if (globalNameTransform && globalNameTransform.formatString != "{0}") {
                    $j('#sourceItemsCount').html(globalSettings.sourceItemsCount + " items to be transformed and saved to another component");
                }
                else if (globalSettings && globalSettings.sourceFolderUri != "tcm:" + globalSettings.targetId) {
                    $j('#sourceItemsCount').html(globalSettings.sourceItemsCount + " items to be transformed and saved to another folder");
                }
                else {
                    $j('#sourceItemsCount').html(globalSettings.sourceItemsCount + " items to be changed schema and saved back");
                }
            }
            else {
                $j('#sourceItemsCount').html("<span class='red'>" + globalSettings.sourceItemsCount + " items found. Please set field mapping below</span>");
            }

        }
        else {
            $j('#sourceItemsCountContainer').hide();
        }

        //main button visibility

        var impersonated = getIsImpersonated(sourceSchemaUri, targetSchemaUri);

        if (impersonated && (sourceSchemaUri == targetSchemaUri || globalMapping || globalMetadataMapping)) {

            $j("#transform").unbind();
            $j("#transform").removeClass("disabled");
            $j("#transform").addClass("enabled");
            $j("#transform.enabled").click(function () {
                startTransform();
            });
        }
        else {

            $j("#transform").removeClass("enabled");
            $j("#transform").addClass("disabled");
            $j("#transform").unbind();
        }

        //popup buttons coloring

        if (globalMapping)
        {
            $j("#fieldMapping").addClass("green");
        }

        if (globalMetadataMapping) {
            $j("#metadataMapping").addClass("green");
        }

        if (globalNameTransform && globalNameTransform.formatString != "{0}") {
            $j("#nameTransform").addClass("green");
        }

        if (globalTargetLinkPath) {
            $j("#targetLink").addClass("green");
        }

        if (globalNameTransform && globalNameTransform.formatString != "{0}" || globalSettings && globalSettings.sourceFolderUri != "tcm:" + globalSettings.targetId) {

            $j("#targetLink").unbind();
            $j("#targetLink").removeClass("disabled");
            $j("#targetLink").addClass("enabled");
            $j("#targetLink").click(function () {
                openTargetLinkDialog();
            });
        }
        else {

            $j("#targetLink").removeClass("enabled");
            $j("#targetLink").addClass("disabled");
            $j("#targetLink").unbind();
        }

        if (sourceSchemaUri == targetSchemaUri) {
            //fix or change schema - localize disabled
            $j("#localizeTarget").prop("disabled", true);
            $j('#localizeTarget').prop('checked', false);
            $j("#localizeTarget").unbind();
        }
        else {
            //localize checkbox - handler
            $j('#localizeTarget').prop("disabled", false);
            $j('#localizeTarget').prop('checked', getCookie(sourceId + '_localizeTarget') == 'true');
            $j("#localizeTarget").unbind();
            $j('#localizeTarget').change(function () {
                setCookie(sourceId + '_localizeTarget', $j(this).is(':checked'), 7);
            });
        }

        if (!impersonated) {
            alert('Please impersonate with Windows account in Alchemy control room');
        }
    }

    function getIsImpersonated(sourceSchemaUri, targetSchemaUri) {

        if (!window.globalSettings)
            return true;

        if (window.globalSettings.isImpersonated)
            return true;

        if (sourceSchemaUri == targetSchemaUri && !destinationChanged())
            return true;

        var sourceSchema = globalSettings.sourceSchemas.filter(function (item) { return item.tcmId == sourceSchemaUri; })[0];
        var targetSchema = globalSettings.targetSchemas.filter(function (item) { return item.tcmId == targetSchemaUri; })[0];

        if (sourceSchema && targetSchema && sourceSchema.schemaType == 1 && targetSchema.schemaType == 1) {
            return false;
        }

        return true;
    }

    function destinationChanged()
    {
        if (!nameTransformDefault(globalNameTransform) || globalSettings && globalSettings.sourceFolderUri != "tcm:" + globalSettings.targetId)
            return true;

        return false;        
    }

})();