﻿Type.registerNamespace("Alchemy4Tridion.Plugins.ComponentTransformer");

!(function () {

    $j = Alchemy.library("jQuery");

    var sourceId = $url.getHashParam("sourceId");

    $j("#close_window.enabled").click(function () {
        closeWindow();
    });

    //window-level mapping object
    var popupNameTransform;

    loadNameTransform(sourceId);

    function loadNameTransform(sourceId) {

        //disable buttons

        $j("#ok").removeClass("enabled");
        $j("#ok").addClass("disabled");
        $j("#ok").unbind();

        // This is the call to my controller
        Alchemy.Plugins["${PluginName}"].Api.ComponentTransformerService.getNameTransform(sourceId).success(function (nameTransform) {

            //console.log(nameTransform);

            //enable buttons

            $j("#ok").removeClass("disabled");
            $j("#ok").addClass("enabled");

            $j("#ok").click(function () {
                saveNameTransform();
            });

            //load form

            var formatString = getCookie('formatString');
            if (!formatString)
                formatString = nameTransform.formatString;
            else
                //save cookie value to object
                nameTransform.formatString = formatString;

            $j('#formatString').val(formatString);

            //handler
            $j('#formatString').change(function () {
                nameTransform.formatString = $j('#formatString').val();
                setCookie('formatString', $j('#formatString').val(), 7);
            });

            $j.each(nameTransform.sourceFields, function (i, item) {

                var space = "";
                for (j = 0; j < item.level; j++) {
                    space += '- ';
                }

                var option = $j('<option>', { value: item.fieldPath, text: space + item.fieldName });

                if (item.primitive == true || item.link == true) { }
                else {
                    option.attr('disabled', 'disabled');
                }

                $j('select').append(option);
            });

            for (i = 0; i < nameTransform.replacements.length; i++) {

                var fieldPath = getCookie('replacement' + i + '_field');
                if (!fieldPath)
                    fieldPath = nameTransform.replacements[i].fieldPath;
                else
                    //save cookie to object
                    nameTransform.replacements[i].fieldPath = fieldPath;

                var regex = getCookie('replacement' + i + '_regex');
                if (!regex)
                    regex = nameTransform.replacements[i].regex;
                else
                    //save cookie to object
                    nameTransform.replacements[i].regex = regex;

                $j('#replacement' + i + ' select').val(fieldPath);
                $j('#replacement' + i + ' select').attr('title', fieldPath);
                $j('#replacement' + i + ' input').val(regex);
            }

            //event handlers

            $j('select').change(function () {
                var sval = $j(this).val();
                var i = $j(this).closest('.replacement').data("id");
                nameTransform.replacements[i].fieldPath = sval;
                $j(this).attr('title', sval);

                setCookie($j(this).closest('.replacement').attr("id") + "_field", $j(this).val(), 7);
            });

            $j('input').change(function () {
                var i = $j(this).closest('.replacement').data("id");
                nameTransform.replacements[i].regex = $j(this).val();

                setCookie($j(this).closest('.replacement').attr("id") + "_regex", $j(this).val(), 7);
            });

            popupNameTransform = nameTransform;
        })
        .error(function (error) {
            console.log("There was an error", error);
            $messages.registerError("An error has occurred", error && error.exceptionMessage ? error.exceptionMessage : null, null, true, false);
        })
        .complete(function () {

        });
    }

    function saveNameTransform() {

        var proceed = true;
        if (nameTransformDefault(popupNameTransform)) {
            proceed = confirm("Save default settings?");
        }
        if (!proceed)
            return;

        opener.globalNameTransform = popupNameTransform;
        window.close();
    }

    function closeWindow() {
        window.close();
    }

})();