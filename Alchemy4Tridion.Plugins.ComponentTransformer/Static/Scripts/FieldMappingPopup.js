﻿Type.registerNamespace("Alchemy4Tridion.Plugins.ComponentTransformer");

!(function () {

    $j = Alchemy.library("jQuery");

    var sourceId = $url.getHashParam("sourceId");
    var targetId = $url.getHashParam("targetId");
    var metadata = $url.getHashParam("metadata");

    $j("#close_window.enabled").click(function () {
        closeWindow();
    });

    //window-level mapping object
    var popupMapping;

    loadFieldMapping(sourceId, targetId, metadata);

    function loadFieldMapping(sourceId, targetId, metadata) {

        //disable buttons

        $j("#ok").removeClass("enabled");
        $j("#ok").addClass("disabled");
        $j("#ok").unbind();

        //enable progress bar
        $j("#progBar").show();

        // This is the call to my controller
        Alchemy.Plugins["${PluginName}"].Api.ComponentTransformerService.getMapping(sourceId, targetId, metadata).success(function (mapping) {

            //console.log(mapping);

            //disable progress bar
            $j("#progBar").hide();

            //enable buttons

            $j("#ok").removeClass("disabled");
            $j("#ok").addClass("enabled");

            $j("#ok").click(function () {
                saveMapping();
            });

            //load form

            var form = '<tr class="headings"><th class="name">Target</th><th class="path">Source</th><th class=\"operation\">Default</th></tr>';

            for (i = 0; i < mapping.fieldMapping.length; i++) {

                form += '<tr class="item" id="' + mapping.fieldMapping[i].id + '" data-id="' + i + '">';

                var space = "";
                for (j = 0; j < mapping.fieldMapping[i].level; j++) {
                    space += '&nbsp; ';
                }

                if (mapping.fieldMapping[i].mandatory) {
                    space += '<span style="color:red">*</span> ';
                }
                else {
                    space += '&nbsp; &nbsp;';
                }

                form += '<td class="name" title="' + mapping.fieldMapping[i].targetFieldPath + '">' + space + mapping.fieldMapping[i].targetFieldName + '</td>';

                form += '<td class="path"><select /></td>';

                form += '<td class="operation"><input' + (mapping.fieldMapping[i].primitive == true || mapping.fieldMapping[i].link == true ? '' : ' readonly class="readonly"') + ' type="text" /></td>';

                form += '</tr>';
            }

            $j("#fieldMappingForm").html(form);

            for (i = 0; i < mapping.fieldMapping.length; i++) {

                //fill with options

                $j.each(mapping.sourceFields, function (optionIndex, item) {

                    var space = "";
                    for (j = 0; j < item.level; j++) {
                        space += '- ';
                    }

                    var enabled = !item.fieldPath ||
                        item.fieldPath == "create_new" ||
                        item.primitive == true && mapping.fieldMapping[i].primitive == true ||
                        item.link == true && mapping.fieldMapping[i].link == true ||
                        item.embedded == true && mapping.fieldMapping[i].embedded == true;

                    var option = $j('<option>', { value: item.fieldPath, text: space + item.fieldName });
                    if (!enabled)
                        option.attr('disabled', 'disabled');

                    $j('#' + mapping.fieldMapping[i].id + ' select').append(option);
                });

                //get value from cookies
                var dval = getCookie(mapping.fieldMapping[i].id);
                //set default value
                if (!dval)
                    dval = mapping.fieldMapping[i].sourceFieldPath;
                else
                    //save cookie value to mapping
                    mapping.fieldMapping[i].sourceFieldPath = dval;

                $j('#' + mapping.fieldMapping[i].id + ' select').val(dval);
                $j('#' + mapping.fieldMapping[i].id + ' select').attr('title', dval);

                var sval = $j('#' + mapping.fieldMapping[i].id + ' select').val();
                mapping.fieldMapping[i].sourceFieldPath = sval;
                if (!sval)
                    $j('#' + mapping.fieldMapping[i].id + ' option')[0].selected = true;

                //field default value

                var defaultValue = getCookie(mapping.fieldMapping[i].id + "_defaultValue");
                if (!defaultValue)
                    defaultValue = mapping.fieldMapping[i].defaultValue;
                else
                    //save cookie value to mapping
                    mapping.fieldMapping[i].defaultValue = defaultValue;

                $j('#' + mapping.fieldMapping[i].id + ' input').val(defaultValue);

            }

            //event handlers

            $j('select').change(function () {
                var sval = $j(this).val();
                var idx = $j(this).closest('.item').data("id");
                mapping.fieldMapping[idx].sourceFieldPath = sval;
                $j(this).attr('title', sval);

                setCookie($j(this).closest('.item').attr("id"), $j(this).val(), 7);

                //set child fields automatically
                for (i = 0; i < mapping.fieldMapping.length; i++) {
                    var prtval = mapping.fieldMapping[idx].targetFieldPath;
                    var cldval = mapping.fieldMapping[i].targetFieldPath;
                    if (cldval.indexOf(prtval) > -1) {

                        var newcldval = cldval.replace(prtval, $j(this).val());
                        $j('#' + mapping.fieldMapping[i].id + ' select').val(newcldval);

                        newcldval = $j('#' + mapping.fieldMapping[i].id + ' select').val();
                        mapping.fieldMapping[i].sourceFieldPath = newcldval;
                        if (newcldval) {
                            $j('#' + mapping.fieldMapping[i].id + ' select').attr('title', newcldval);
                            setCookie(mapping.fieldMapping[i].id, newcldval, 7);
                        }
                        else {
                            $j('#' + mapping.fieldMapping[i].id + ' option')[0].selected = true;
                        }
                    }
                }
            });

            $j('input').change(function () {
                var idx = $j(this).closest('.item').data("id");
                mapping.fieldMapping[idx].defaultValue = $j(this).val();

                setCookie($j(this).closest('.item').attr("id") + "_defaultValue", $j(this).val(), 7);
            });

            popupMapping = mapping;

        })
        .error(function (error) {
            console.log("There was an error", error);
            $messages.registerError("An error has occurred", error && error.exceptionMessage ? error.exceptionMessage : null, null, true, false);
        })
        .complete(function () {

        });
    }

    function saveMapping() {

        var proceed = true;
        var message = getValidationMessage(popupMapping);
        if (message) {
            proceed = confirm(message + "\nAre you sure?");
        }
        if (!proceed)
            return;

        var metadata = $url.getHashParam("metadata");

        if (metadata == "true")
        {
            opener.globalMetadataMapping = popupMapping;
        }
        else
        {
            opener.globalMapping = popupMapping;
        }

        window.close();
    }

    function closeWindow() {
        window.close();
    }

    function getValidationMessage(mapping) {

        var res = "";

        console.log(mapping);

        for (i = 0; i < mapping.fieldMapping.length; i++) {
            
            var mappingItem = mapping.fieldMapping[i];

            //mandatory field not mapped
            if (mappingItem.mandatory && !mappingItem.defaultValue && !mappingItem.sourceFieldPath) {
                res += "Mandatory field " + mappingItem.targetFieldPath + " is not mapped\n";
            }
            else {

                //numeric field is mapped to string field
                if (mappingItem.fieldType == "Number" && mappingItem.sourceFieldPath) {
                    for (j = 0; j < mapping.sourceFields.length; j++) {
                        if (mapping.sourceFields[j].fieldPath == mappingItem.sourceFieldPath && mapping.sourceFields[j].fieldType != "Number") {
                            res += "Numeric field " + mappingItem.targetFieldPath + " is mapped to field of other type\n";
                        }
                    }
                }

                //string field is mapped to xhtml field
                if ((mappingItem.fieldType == "SingleLineText" || mappingItem.fieldType == "MultiLineText") && mappingItem.sourceFieldPath) {
                    for (j = 0; j < mapping.sourceFields.length; j++) {
                        if (mapping.sourceFields[j].fieldPath == mappingItem.sourceFieldPath && mapping.sourceFields[j].fieldType == "Xhtml") {
                            res += "String field " + mappingItem.targetFieldPath + " is mapped to Xhtml. Formatting will be lost\n";
                        }
                    }
                }

                //numeric field has string default value
                if (mappingItem.fieldType == "Number" && mappingItem.defaultValue && !/^\d+$/i.test(mappingItem.defaultValue)) {
                    res += "Numeric field " + mappingItem.targetFieldPath + " has non-numeric default value\n";
                }

                //link or componnet link field has non-tcm value
                if (mappingItem.link && mappingItem.defaultValue && !/^tcm:(\d+)-(\d+)$/i.test(mappingItem.defaultValue)) {
                    res += "Component/multimedia link field " + mappingItem.targetFieldPath + " has non-tcm default value\n";
                }

                //single-value field mapped to multi-value
                if (mappingItem.multiValue == false && mappingItem.sourceFieldPath) {
                    for (j = 0; j < mapping.sourceFields.length; j++) {
                        if (mapping.sourceFields[j].fieldPath == mappingItem.sourceFieldPath && mapping.sourceFields[j].multiValue) {
                            res += "Single-value field " + mappingItem.targetFieldPath + " is mapped to multi-value field. Elements with index > 0 will be lost\n";
                        }
                    }
                }

                //mandatory field mapped to non-mandatory
                if (mappingItem.mandatory && mappingItem.sourceFieldPath && !mappingItem.defaultValue) {
                    for (j = 0; j < mapping.sourceFields.length; j++) {
                        if (mapping.sourceFields[j].fieldPath == mappingItem.sourceFieldPath && !mapping.sourceFields[j].mandatory) {
                            res += "Mandatory field " + mappingItem.targetFieldPath + " is mapped to non-mandatory field\n";
                        }
                    }
                }

            }
        }

        return res;
    }

})();