﻿/**
 * Creates an anguilla command using a wrapper shorthand.
 *
 * Note the ${PluginName} will get replaced by the actual plugin name.
 */
Alchemy.command("${PluginName}", "ComponentTransformer", {

    /**
     * If an init function is created, this will be called from the command's constructor when a command instance
     * is created.
     */
    init: function () {
        console.log("Init ComponentTransformer");
    },

    /**
     * Whether or not the command is enabled for the user (will usually have extensions displayed but disabled).
     * @returns {boolean}
     */
    isEnabled: function (selection) {

        var count = selection.getCount();
        if (!count)
            return false;

        for (var i = 0; i < count; i++) {

            var itemUri = selection.getItem(i);
            if (!itemUri)
                continue;

            var item = $models.getItem(itemUri);

            var containerUri = item.getOrganizationalItemId();
            if (containerUri.startsWith("tcm:0-")) // exclude top folder and SG
                return false;

            var itemType = $models.getItemType(itemUri);

            if (itemType === $const.ItemType.COMPONENT
                || itemType === $const.ItemType.FOLDER
                || itemType === $const.ItemType.PAGE
                || itemType === $const.ItemType.STRUCTURE_GROUP
                || itemType === $const.ItemType.KEYWORD
                || itemType === $const.ItemType.CATEGORY) { }
            else {
                return false;
            }
        }

        return true;
    },


    /**
     * Whether or not the command is available to the user.
     * @returns {boolean}
     */
    isAvailable: function (selection) {

        var count = selection.getCount();
        if (!count)
            return false;

        for (var i = 0; i < count; i++) {

            var itemUri = selection.getItem(i);
            if (!itemUri)
                continue;

            var itemType = $models.getItemType(itemUri);

            if (itemType === $const.ItemType.COMPONENT
                || itemType === $const.ItemType.FOLDER
                || itemType === $const.ItemType.PAGE
                || itemType === $const.ItemType.STRUCTURE_GROUP
                || itemType === $const.ItemType.KEYWORD
                || itemType === $const.ItemType.CATEGORY) { }
            else {
                return false;
            }
        }

        return true;
    },

    execute: function (selection) {

        var itemUri = selection.getItem(0);
        var url = "${ViewsUrl}ComponentTransformerPopup.aspx#id={0}";
        var popup = $popupManager.createExternalContentPopup(url.format(itemUri), "width=700px,height=450px,resizable=0");

        $evt.addEventHandler(popup, "unload", function (e) {
            var popup = e.source;
            if (popup) {
                popup.dispose();
                popup = null;
            }
        });

        popup.open();
    }

});