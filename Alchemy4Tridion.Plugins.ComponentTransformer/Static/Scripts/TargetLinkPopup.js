﻿Type.registerNamespace("Alchemy4Tridion.Plugins.ComponentTransformer");

!(function () {

    $j = Alchemy.library("jQuery");

    var sourceId = $url.getHashParam("sourceId");

    $j("#close_window.enabled").click(function () {
        closeWindow();
    });

    $j('#targetLinkOverwrite').prop('checked', getCookie('targetLinkOverwrite') == 'true');

    //window-level mapping object
    var popupTargetLinkPath;

    loadTargetLink(sourceId);

    function loadTargetLink(sourceId) {

        //disable buttons

        $j("#ok").removeClass("enabled");
        $j("#ok").addClass("disabled");
        $j("#ok").unbind();

        // This is the call to my controller
        Alchemy.Plugins["${PluginName}"].Api.ComponentTransformerService.getTargetLink(sourceId).success(function (info) {

            //disable progress bar
            $j("#progBar").hide();

            //enable buttons

            $j("#ok").removeClass("disabled");
            $j("#ok").addClass("enabled");

            $j("#ok").click(function () {
                saveTargetLink();
            });

            //fill with options

            $j.each(info.sourceFields, function (i, item) {

                var space = "";
                for (j = 0; j < item.level; j++) {
                    space += '- ';
                }

                var option = $j('<option>', { value: item.fieldPath, text: space + item.fieldName });

                if (item.fieldPath == '' || item.link == true) { }
                else {
                    option.attr('disabled', 'disabled');
                }

                $j('select').append(option);
            });

            //get value from cookies
            var dval = getCookie('targetLink');
            $j('select').val(dval);
            $j('select').attr('title', dval);

            var sval = $j('select').val();
            if (!sval)
                $j('option')[0].selected = true;

            popupTargetLinkPath = $j('select').val();

            //event handlers

            $j('select').change(function () {

                popupTargetLinkPath = $j(this).val();
                $j(this).attr('title', popupTargetLinkPath);

                setCookie('targetLink', $j(this).val(), 7);
            });

            $j('#targetLinkOverwrite').change(function () {
                setCookie('targetLinkOverwrite', $j(this).is(':checked'), 7);
            });

        })
        .error(function (error) {
            console.log("There was an error", error);
            $messages.registerError("An error has occurred", error && error.exceptionMessage ? error.exceptionMessage : null, null, true, false);
        })
        .complete(function () {

        });
    }

    function saveTargetLink() {
        opener.globalTargetLinkPath = popupTargetLinkPath;
        opener.globalTargetLinkOverwrite = $j('#targetLinkOverwrite').is(":checked");
        window.close();
    }

    function closeWindow() {
        window.close();
    }

})();