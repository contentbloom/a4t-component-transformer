﻿//$JQ = Alchemy.library("jQuery");

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function deleteCookie(cname) {
    document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

function refreshDashboard(itemId) {
    (function (UI, B, e, a, r, d) {
        try {
            while (r = UI.frames[e++]) {
                if ((a = r.$display && r.$display.getView()) && a.getId() === B) {
                    d = a;
                    break;
                }
            }

            if (itemId) {
                r.$display.getView().navigateTo(itemId, true, opener);
            }
            else {
                r.$display.getView().refreshList();
            }

        } catch (x) {
        };
    })(window.opener.top, 'DashboardView', 0);
}

function nameTransformDefault(nameTransform) {
    return !nameTransform || nameTransform.formatString == "{0}" && nameTransform.replacements[0].fieldPath == "[Title]" && (nameTransform.replacements[0].regex == ".+" || !nameTransform.replacements[0].regex)
}