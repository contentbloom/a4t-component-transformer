﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Alchemy4Tridion.Plugins.ComponentTransformer.Helpers;
using Alchemy4Tridion.Plugins.ComponentTransformer.Models;
using Alchemy4Tridion.Plugins.ComponentTransformer.ViewModels;
using Alchemy4Tridion.Plugins.Clients.CoreService;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.Controllers
{
    [AlchemyRoutePrefix("ComponentTransformerService")]
    public class ComponentTransformerController : AlchemyApiController
    {
        [HttpGet]
        [Route("Settings/{sourceId}/{targetId}")]
        public Settings GetSettings(string sourceId, string targetId)
        {
            try
            {
                ItemType sourceItemType = MainHelper.GetItemType(sourceId);

                string sourceWebDav = null;
                string targetWebDav = null;

                string defaultSchemaUri = null;

                RepositoryLocalObjectData sourceItem = (RepositoryLocalObjectData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId);

                if (sourceItem != null)
                {
                    sourceWebDav = sourceItem.GetWebDav();

                    if (sourceId == targetId && MainHelper.GetItemType(sourceId) == ItemType.Component)
                    {
                        targetId = MainHelper.GetItemContainer(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId).Replace("tcm:", "");
                    }

                    RepositoryLocalObjectData targetItem = (RepositoryLocalObjectData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + targetId);

                    if (sourceItem != null && targetItem == null)
                    {
                        targetId = MainHelper.GetItemContainer(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId).Replace("tcm:", "");
                        targetItem = (RepositoryLocalObjectData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + targetId);
                    }

                    if (targetItem == null || MainHelper.GetItemType(targetId) != ItemType.Folder)
                    {
                        ItemInfo item = MainHelper.GetContainersByPublication(this.Clients.SessionAwareCoreServiceClient, MainHelper.GetPublicationTcmId("tcm:" + sourceId)).FirstOrDefault(x => x.ItemType == ItemType.Folder);
                        if (item != null)
                        {
                            targetId = item.TcmId.Replace("tcm:", "");
                            targetItem = (RepositoryLocalObjectData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + targetId);
                        }
                    }

                    if (targetItem != null)
                    {
                        targetWebDav = targetItem.GetWebDav();
                    }

                    if (sourceItemType == ItemType.Component)
                    {
                        //take component schema
                        ComponentData component = (ComponentData)sourceItem;
                        defaultSchemaUri = component.Schema.IdRef;
                    }
                    if (sourceItemType == ItemType.Folder)
                    {
                        //take folder linked schema
                        FolderData folder = (FolderData)sourceItem;
                        defaultSchemaUri = folder.LinkedSchema.IdRef;

                        //take folder first component schema
                        if (string.IsNullOrEmpty(defaultSchemaUri) || defaultSchemaUri == "tcm:0-0-0")
                        {
                            List<ItemInfo> components = MainHelper.GetComponents(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId, true);
                            if (components.Any())
                            {
                                ComponentData component = (ComponentData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, components.First().TcmId);
                                defaultSchemaUri = component.Schema.IdRef;
                            }
                        }
                    }

                    //take matadata schema
                    if (string.IsNullOrEmpty(defaultSchemaUri) || defaultSchemaUri == "tcm:0-0-0")
                    {
                        defaultSchemaUri = sourceItem.MetadataSchema.IdRef;
                    }

                    if (defaultSchemaUri == "tcm:0-0-0")
                    {
                        defaultSchemaUri = null;
                    }
                }

                List<ItemInfo> sourceSchemas = MainHelper.GetSchemas(this.Clients.SessionAwareCoreServiceClient, MainHelper.GetPublicationTcmId("tcm:" + sourceId))
                    .Where(x => x.SchemaType == SchemaType.Component || x.SchemaType == SchemaType.Multimedia || x.SchemaType == SchemaType.Metadata).OrderBy(x => x.Title).ToList();

                List<ItemInfo> targetSchemas = MainHelper.GetSchemas(this.Clients.SessionAwareCoreServiceClient, MainHelper.GetPublicationTcmId("tcm:" + targetId))
                    .Where(x => x.SchemaType == SchemaType.Component || x.SchemaType == SchemaType.Multimedia).OrderBy(x => x.Title).ToList();

                List<ItemInfo> sourceItems = new List<ItemInfo>();
                if (sourceItemType == ItemType.Folder || sourceItemType == ItemType.StructureGroup || sourceItemType == ItemType.Category)
                {
                    //get list of source items to be transformed
                    sourceItems = MainHelper.GetItemsByParentContainer(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId, true, new ItemType[] { ItemType.Component, ItemType.Page, ItemType.StructureGroup, ItemType.Keyword });
                }
                else
                {
                    //add item to be transformed
                    sourceItems.Add(((RepositoryLocalObjectData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId)).ToItem());
                }

                string sourceFolderUri = "tcm:" + sourceId;
                if (sourceItemType != ItemType.Folder && sourceItemType != ItemType.StructureGroup && sourceItemType != ItemType.Category)
                {
                    sourceFolderUri = MainHelper.GetItemContainer(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId);
                }

                string targetFolderUri = MainHelper.GetItemContainer(this.Clients.SessionAwareCoreServiceClient, "tcm:" + targetId);

                ItemType targetItemType = ItemType.Folder;

                Settings settings = new Settings
                {
                    SourceId = sourceId,
                    TargetId = targetId,
                    SourceFolderUri = sourceFolderUri,
                    TargetFolderUri = targetFolderUri,
                    SourceWebdav = sourceWebDav,
                    TargetWebdav = targetWebDav,
                    SourceIcon = "T" + (int)sourceItemType + "L0P0",
                    TargetIcon = "T" + (int)targetItemType + "L0P0",
                    SourceSchemas = sourceSchemas,
                    TargetSchemas = targetSchemas,
                    SourceSchemaUri = !string.IsNullOrEmpty(defaultSchemaUri) && sourceSchemas.Any(x => x.TcmId.GetId() == defaultSchemaUri.GetId()) ? defaultSchemaUri : null,
                    TargetSchemaUri = !string.IsNullOrEmpty(defaultSchemaUri) && targetSchemas.Any(x => x.TcmId.GetId() == defaultSchemaUri.GetId()) ? defaultSchemaUri : null,
                    SourceItems = sourceItems,
                    SourceItemsCount = sourceItems.Count,
                    IsImpersonated = this.Clients.IsImpersonationUserSet()
                };

                return settings;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("MetadataFieldsCount/{schemaId}")]
        public int GetMetadataFieldsCount(string schemaId)
        {
            try
            {
                var fields = MainHelper.GetSchemaMetadataFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + schemaId);
                if (fields == null)
                    return 0;
                return fields.Count;                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("Mapping/{sourceId}/{targetId}/{metadata}")]
        public Mapping GetMapping(string sourceId, string targetId, bool metadata)
        {
            try
            {
                Mapping mapping = new Mapping();

                List<ItemFieldDefinitionData> sourceComponentFields = MainHelper.GetSchemaFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId) ?? new List<ItemFieldDefinitionData>();
                List<ItemFieldDefinitionData> sourceMetadataFields = MainHelper.GetSchemaMetadataFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId);
                SchemaData sourceSchema = (SchemaData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId);
                List<Models.FieldInfo> sourceFields = MainHelper.GetAllFields(this.Clients.SessionAwareCoreServiceClient, sourceSchema, sourceComponentFields, sourceMetadataFields, true);

                mapping.SourceFields = sourceFields.Select(x => new ViewModels.FieldInfo {
                    FieldName = x.FieldName,
                    FieldPath = x.FieldPath,
                    Level = x.Level,
                    Primitive = x.Primitive,
                    Embedded = x.Embedded,
                    Link = x.Link,
                    Mandatory = x.Mandatory,
                    MultiValue = x.MultiValue,
                    FieldType = x.FieldType
                }).ToList();

                List<ItemFieldDefinitionData> targetComponentFields = MainHelper.GetSchemaFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + targetId) ?? new List<ItemFieldDefinitionData>();
                List<ItemFieldDefinitionData> targetMetadataFields = MainHelper.GetSchemaMetadataFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + targetId);
                SchemaData targetSchema = (SchemaData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + targetId);

                mapping.FieldMapping = MainHelper.GetDefaultFieldMapping(this.Clients.SessionAwareCoreServiceClient, targetSchema, !metadata ? targetComponentFields : null, metadata ? targetMetadataFields : null)
                    .Select(x => new ViewModels.FieldMappingInfo
                    {
                        Id = x.Id,
                        SourceFieldPath = x.SourceFieldPath,
                        TargetFieldName = x.TargetFieldName,
                        TargetFieldPath = x.TargetFieldPath,
                        DefaultValue = x.DefaultValue,
                        Level = x.Level,
                        Primitive = x.Primitive,
                        Embedded = x.Embedded,
                        Link = x.Link,
                        Mandatory = x.Mandatory,
                        MultiValue = x.MultiValue,
                        FieldType = x.FieldType,
                        Index = x.Index
                    }).ToList();

                return mapping;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("NameTransform/{sourceId}")]
        public NameTransform GetNameTransform(string sourceId)
        {
            try
            {
                NameTransform nameTransform = new NameTransform();
                nameTransform.FormatString = "{0}";

                nameTransform.Replacements = new List<ReplacementInfo>();
                nameTransform.Replacements.Add(new ReplacementInfo() { FieldPath = "[Title]", Regex = ".+" });
                nameTransform.Replacements.Add(new ReplacementInfo() { FieldPath = "", Regex = "" });
                nameTransform.Replacements.Add(new ReplacementInfo() { FieldPath = "", Regex = "" });

                nameTransform.SourceFields = new List<ViewModels.FieldInfo>();
                nameTransform.SourceFields.Add(new ViewModels.FieldInfo { FieldName = "< ignore >", FieldPath = "", Level = 0, Primitive = true });
                nameTransform.SourceFields.Add(new ViewModels.FieldInfo { FieldName = "[Title]", FieldPath = "[Title]", Level = 0, Primitive = true });
                nameTransform.SourceFields.Add(new ViewModels.FieldInfo { FieldName = "[TcmId]", FieldPath = "[TcmId]", Level = 0, Primitive = true });
                nameTransform.SourceFields.Add(new ViewModels.FieldInfo { FieldName = "[ID]", FieldPath = "[ID]", Level = 0, Primitive = true });

                List<ItemFieldDefinitionData> sourceComponentFields = MainHelper.GetSchemaFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId) ?? new List<ItemFieldDefinitionData>();
                List<ItemFieldDefinitionData> sourceMetadataFields = MainHelper.GetSchemaMetadataFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId);
                SchemaData sourceSchema = (SchemaData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId);
                List<Models.FieldInfo> sourceFields = MainHelper.GetAllFields(this.Clients.SessionAwareCoreServiceClient, sourceSchema, sourceComponentFields, sourceMetadataFields, false);

                nameTransform.SourceFields.AddRange(sourceFields.Select(x => new ViewModels.FieldInfo {
                    FieldName = x.FieldName,
                    FieldPath = x.FieldPath,
                    Level = x.Level,
                    Primitive = x.Primitive,
                    Embedded = x.Embedded,
                    Link = x.Link,
                    Mandatory = x.Mandatory,
                    MultiValue = x.MultiValue,
                    FieldType = x.FieldType
                }));

                return nameTransform;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("TargetLink/{sourceId}")]
        public TargetLink GetTargetLink(string sourceId)
        {
            try
            {
                TargetLink targetLink = new TargetLink();

                targetLink.SourceFields = new List<ViewModels.FieldInfo>();
                targetLink.SourceFields.Add(new ViewModels.FieldInfo { FieldName = "< ignore >", FieldPath = "", Level = 0, Primitive = true });

                List<ItemFieldDefinitionData> sourceComponentFields = MainHelper.GetSchemaFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId) ?? new List<ItemFieldDefinitionData>();
                List<ItemFieldDefinitionData> sourceMetadataFields = MainHelper.GetSchemaMetadataFields(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId);
                SchemaData sourceSchema = (SchemaData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, "tcm:" + sourceId);
                List<Models.FieldInfo> sourceFields = MainHelper.GetAllFields(this.Clients.SessionAwareCoreServiceClient, sourceSchema, sourceComponentFields, sourceMetadataFields, false);

                targetLink.SourceFields.AddRange(sourceFields.Select(x => new ViewModels.FieldInfo
                {
                    FieldName = x.FieldName,
                    FieldPath = x.FieldPath,
                    Level = x.Level,
                    Primitive = x.Primitive,
                    Embedded = x.Embedded,
                    Link = x.Link,
                    Mandatory = x.Mandatory,
                    MultiValue = x.MultiValue,
                    FieldType = x.FieldType
                }));

                return targetLink;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("Transform")]
        public List<ResultInfo> Transform(Transform transform)
        {
            try
            {
                List<ResultInfo> results = new List<ResultInfo>();

                string sourceUri = transform.SourceUri;
                string sourceFolderUri = transform.SourceFolderUri;
                string targetFolderUri = transform.TargetFolderUri;
                string sourceSchemaUri = transform.SourceSchemaUri;
                string targetSchemaUri = transform.TargetSchemaUri;

                ItemType sourceItemType = MainHelper.GetItemType(sourceUri);

                bool localize = transform.Localize;

                List<ItemFieldDefinitionData> sourceComponentFields = MainHelper.GetSchemaFields(this.Clients.SessionAwareCoreServiceClient, sourceSchemaUri) ?? new List<ItemFieldDefinitionData>();
                List<ItemFieldDefinitionData> sourceMetadataFields = MainHelper.GetSchemaMetadataFields(this.Clients.SessionAwareCoreServiceClient, sourceSchemaUri);
                SchemaData sourceSchema = (SchemaData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, sourceSchemaUri);
                List<Models.FieldInfo> allSourceFields = MainHelper.GetAllFields(this.Clients.SessionAwareCoreServiceClient, sourceSchema, sourceComponentFields, sourceMetadataFields, false);

                List<ItemFieldDefinitionData> targetComponentFields = MainHelper.GetSchemaFields(this.Clients.SessionAwareCoreServiceClient, targetSchemaUri) ?? new List<ItemFieldDefinitionData>();
                List<ItemFieldDefinitionData> targetMetadataFields = MainHelper.GetSchemaMetadataFields(this.Clients.SessionAwareCoreServiceClient, targetSchemaUri);
                SchemaData targetSchema = (SchemaData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, targetSchemaUri);
                List<Models.FieldInfo> allTargetFields = MainHelper.GetAllFields(this.Clients.SessionAwareCoreServiceClient, targetSchema, targetComponentFields, targetMetadataFields, false);

                List<Models.FieldMappingInfo> fieldMapping = GetMappingModels(transform.ComponentFieldMapping, allSourceFields, allTargetFields, "0", 0);
                List<Models.FieldMappingInfo> metadataFieldMapping = GetMappingModels(transform.MetadataFieldMapping, allSourceFields, allTargetFields, "m.0", 0);

                string formatString =  string.IsNullOrEmpty(transform.FormatString) ? "{0}" : transform.FormatString;

                List<ReplacementInfo> replacements = transform.Replacements == null ? new List<ReplacementInfo> { new ReplacementInfo { FieldPath = "[Title]", Regex = ".+" } } :
                    transform.Replacements.Select(x => new ReplacementInfo { FieldPath = x.FieldPath, Regex = x.Regex }).ToList();

                RepositoryLocalObjectData item = (RepositoryLocalObjectData)MainHelper.ReadItem(this.Clients.SessionAwareCoreServiceClient, sourceUri);

                string newTitle = MainHelper.GetTransformedName(this.Clients.SessionAwareCoreServiceClient, sourceUri, formatString, replacements);

                // same folder and same target title: schema change and component fix functionality
                if (sourceFolderUri == targetFolderUri && item.Title == newTitle)
                {
                    // component
                    if (sourceItemType == ItemType.Component)
                    {
                        if (sourceSchemaUri.GetId() == targetSchemaUri.GetId())
                        {
                            // fix component
                            MainHelper.FixComponent(this.Clients.SessionAwareCoreServiceClient, sourceUri, sourceSchemaUri, targetFolderUri, localize, fieldMapping, metadataFieldMapping, results);
                        }
                        else
                        {
                            // change schema for component
                            MainHelper.ChangeSchemaForComponent(this.Clients.SessionAwareCoreServiceClient, sourceUri, sourceSchemaUri, targetSchemaUri, targetFolderUri, localize, fieldMapping, metadataFieldMapping, results);
                        }
                    }

                    // page, sg, keyword, etc
                    else
                    {
                        if (sourceSchemaUri.GetId() == targetSchemaUri.GetId())
                        {
                            // fix page metadata
                            MainHelper.FixTridionObjectMetadata(this.Clients.SessionAwareCoreServiceClient, sourceUri, sourceSchemaUri, targetFolderUri, localize, metadataFieldMapping, results);
                        }
                        else
                        {
                            // change metadata schema for page
                            MainHelper.ChangeMetadataSchemaForTridionObject(this.Clients.SessionAwareCoreServiceClient, sourceUri, sourceSchemaUri, targetFolderUri, targetSchemaUri, localize, metadataFieldMapping, results);
                        }
                    }
                }

                // transform and copy to destination
                else
                {
                    // component
                    if (sourceItemType == ItemType.Component)
                    {
                        // copy / transform component
                        MainHelper.TransformComponent(this.Clients.SessionAwareCoreServiceClient, this.Clients.SessionAwareCoreServiceDownloadClient, this.Clients.SessionAwareCoreServiceUploadClient, sourceUri, sourceFolderUri, sourceSchemaUri, targetFolderUri, targetSchemaUri, newTitle, localize, fieldMapping, metadataFieldMapping, transform.TargetLink, results);
                    }

                    // page, sg, keyword, etc
                    else
                    {
                        // copy / transform metadata
                        MainHelper.TransformTridionObjectMetadata(this.Clients.SessionAwareCoreServiceClient, sourceUri, sourceFolderUri, sourceSchemaUri, targetFolderUri, targetSchemaUri, newTitle, localize, fieldMapping, metadataFieldMapping, transform.TargetLink, results);
                    }
                }

                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<Models.FieldMappingInfo> GetMappingModels(List<ViewModels.FieldMappingInfo> fieldMapping, List<Models.FieldInfo> allSourceFields, List<Models.FieldInfo> allTargetFields, string parentIndex, int level)
        {
            if (fieldMapping == null || string.IsNullOrEmpty(parentIndex))
                return null;

            List<ViewModels.FieldMappingInfo> levelMapping = fieldMapping.Where(x => x.Level == level && (level == 0 || level > 0 && x.Index.StartsWith(parentIndex))).ToList();
            if(levelMapping.Count() == 0)
                return null;

            List<Models.FieldMappingInfo> res = new List<Models.FieldMappingInfo>();
            foreach (ViewModels.FieldMappingInfo mapping in levelMapping)
            {
                res.Add(new Models.FieldMappingInfo
                {
                    SourceFields = allSourceFields,
                    Id = mapping.Id,
                    SourceFieldPath = mapping.SourceFieldPath,
                    TargetFields = allTargetFields,
                    TargetFieldName = mapping.TargetFieldName,
                    TargetFieldPath = mapping.TargetFieldPath,
                    DefaultValue = mapping.DefaultValue,
                    Level = mapping.Level,
                    Primitive = mapping.Primitive,
                    Embedded = mapping.Embedded,
                    Link = mapping.Link,
                    Mandatory = mapping.Mandatory,
                    MultiValue = mapping.MultiValue,
                    FieldType = mapping.FieldType,
                    Index = mapping.Index,

                    ChildFieldMapping = GetMappingModels(fieldMapping, allSourceFields, allTargetFields, mapping.Index, level + 1)
                });
            }

            return res;
        }
    }
}