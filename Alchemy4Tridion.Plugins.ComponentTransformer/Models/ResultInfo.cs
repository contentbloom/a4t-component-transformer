﻿using Alchemy4Tridion.Plugins.ComponentTransformer.Helpers;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.Models
{
    public class ResultInfo
    {
        public ItemInfo Item { get; set; }

        public string Message { get; set; }

        public Status Status { get; set; }

        public string ErrorMessage { get; set; }

    }
}