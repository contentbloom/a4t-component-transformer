﻿namespace Alchemy4Tridion.Plugins.ComponentTransformer.Models
{
    public class ReplacementInfo
    {
        public string FieldPath { get; set; }

        public string Regex { get; set; }
    }
}
