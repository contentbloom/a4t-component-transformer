﻿using System.Collections.Generic;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.Models
{
    public class Settings
    {
        public string SourceId { get; set; }

        public string SourceWebdav { get; set; }

        public string SourceIcon { get; set; }

        public string SourceFolderUri { get; set; }

        public string TargetId { get; set; }

        public string TargetWebdav { get; set; }

        public string TargetIcon { get; set; }

        public string TargetFolderUri { get; set; }

        public List<ItemInfo> SourceSchemas { get; set; }

        public List<ItemInfo> TargetSchemas { get; set; }

        public string SourceSchemaUri { get; set; }

        public string TargetSchemaUri { get; set; }

        public List<ItemInfo> SourceItems { get; set; }

        public int SourceItemsCount { get; set; }

        public bool IsImpersonated { get; set; }
    }
}