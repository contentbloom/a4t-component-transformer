﻿namespace Alchemy4Tridion.Plugins.ComponentTransformer.Models
{
    public class TargetLinkInfo
    {
        public string Path { get; set; }

        public bool Overwrite { get; set; }
    }
}
