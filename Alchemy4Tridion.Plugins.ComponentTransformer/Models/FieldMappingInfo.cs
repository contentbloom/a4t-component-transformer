﻿using System.Collections.Generic;
using System.Linq;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.Models
{
    public class FieldMappingInfo
    {
        public string Id { get; set; }

        public List<FieldInfo> SourceFields { private get; set; }

        public List<FieldInfo> TargetFields { private get; set; }

        public string SourceFieldPath { get; set; }

        public FieldInfo SourceField
        {
            get
            {
                return string.IsNullOrEmpty(this.SourceFieldPath) || this.SourceFields == null ? null : this.SourceFields.FirstOrDefault(x => x.FieldPath == this.SourceFieldPath);
            }
        }

        public string TargetFieldName { get; set; }

        public string TargetFieldPath { get; set; }

        public List<FieldMappingInfo> ChildFieldMapping { get; set; }

        public FieldInfo TargetField
        {
            get
            {
                return string.IsNullOrEmpty(this.TargetFieldPath) || this.TargetFields == null ? null : this.TargetFields.FirstOrDefault(x => x.FieldPath == this.TargetFieldPath);
            }
        }

        public string DefaultValue { get; set; }

        public int Level { get; set; }

        public bool Primitive { get; set; }

        public bool Embedded { get; set; }

        public bool Link { get; set; }

        public bool Mandatory { get; set; }

        public bool MultiValue { get; set; }

        public string FieldType { get; set; }

        public string Index { get; set; }
    }
}
