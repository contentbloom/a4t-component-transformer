﻿using System.Collections.Generic;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.ViewModels
{
    public class TargetLink
    {
        public List<FieldInfo> SourceFields { get; set; }
    }
}