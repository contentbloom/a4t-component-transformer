﻿namespace Alchemy4Tridion.Plugins.ComponentTransformer.ViewModels
{
    public class FieldMappingInfo
    {
        public string Id { get; set; }

        public string SourceFieldPath { get; set; }

        public string TargetFieldName { get; set; }

        public string TargetFieldPath { get; set; }

        public string DefaultValue { get; set; }

        public int Level { get; set; }

        public bool Primitive { get; set; }

        public bool Embedded { get; set; }

        public bool Link { get; set; }

        public bool Mandatory { get; set; }

        public bool MultiValue { get; set; }

        public string FieldType { get; set; }

        public string Index { get; set; }
    }
}