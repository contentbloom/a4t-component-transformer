﻿namespace Alchemy4Tridion.Plugins.ComponentTransformer.ViewModels
{
    public class FieldInfo
    {
        public string FieldName { get; set; }

        public string FieldPath { get; set; }

        public int Level { get; set; }

        public bool Primitive { get; set; }

        public bool Embedded { get; set; }

        public bool Link { get; set; }

        public bool Mandatory { get; set; }

        public bool MultiValue { get; set; }

        public string FieldType { get; set; }
    }
}