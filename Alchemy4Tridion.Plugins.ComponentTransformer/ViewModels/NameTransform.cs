﻿using Alchemy4Tridion.Plugins.ComponentTransformer.Models;
using System.Collections.Generic;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.ViewModels
{
    public class NameTransform
    {
        public string FormatString { get; set; }

        public List<FieldInfo> SourceFields { get; set; }

        public List<ReplacementInfo> Replacements { get; set; }
    }
}