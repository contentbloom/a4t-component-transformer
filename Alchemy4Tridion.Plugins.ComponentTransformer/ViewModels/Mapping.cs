﻿using System.Collections.Generic;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.ViewModels
{
    public class Mapping
    {
        public List<FieldMappingInfo> FieldMapping { get; set; }

        public List<FieldInfo> SourceFields { get; set; }
    }
}