﻿using Alchemy4Tridion.Plugins.ComponentTransformer.Models;
using System.Collections.Generic;

namespace Alchemy4Tridion.Plugins.ComponentTransformer.ViewModels
{
    public class Transform
    {
        public string SourceUri { get; set; }

        public string SourceFolderUri { get; set; }

        public string TargetFolderUri { get; set; }

        public string SourceSchemaUri { get; set; }

        public string TargetSchemaUri { get; set; }

        public string FormatString { get; set; }

        public List<ReplacementInfo> Replacements { get; set; }

        public List<FieldMappingInfo> ComponentFieldMapping { get; set; }

        public List<FieldMappingInfo> MetadataFieldMapping { get; set; }

        public TargetLinkInfo TargetLink { get; set; }

        public bool Localize { get; set; }
    }
}